import {Component, Input, OnInit} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {Player} from 'src/app/models/player';
import {Team} from 'src/app/models/team';
import {PlayerService} from 'src/app/services/player.service';
import { TeamService } from 'src/app/services/team.service';
import {HireNewPlayerDialogComponent} from '../hire-new-player-dialog/hire-new-player-dialog.component';
import {HireVeteranPlayerDialogComponent} from '../hire-veteran-player-dialog/hire-veteran-player-dialog.component';

@Component({
  selector: 'app-team-management-dashboard',
  templateUrl: './team-management-dashboard.component.html',
  styleUrls: ['./team-management-dashboard.component.css']
})
export class TeamManagementDashboardComponent implements OnInit {

  // TODO remove default
  @Input() team!: Team;

  players!: Player[];

  refreshed: boolean = false;

  selectedPlayerID: number = -1;

  constructor(
    private playerService: PlayerService,
    private dialog: MatDialog,
    private teamService: TeamService
  ) {
  }

  ngOnInit(): void {
    this.getTeam();
  }

  getTeam(): void {
    this.teamService.getAssociatedTeam().subscribe(
      team => {
        this.team = team;
        this.getPlayers();
      }
      );
  }

  getPlayers() {
    this.playerService.getPlayersForTeam(this.team.id)
      .subscribe(players => this.players = players);
  }

  onSelectPlayerRow(id: number): void {
    this.selectedPlayerID = id;
  }

  handleHireNewPlayerClick() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      team: this.team
    };
    const dialogRef = this.dialog.open(HireNewPlayerDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(data => this.onHireNewPlayerClose(data));
  }

  handleHireVeteranPlayerClick() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      team: this.team
    };
    const dialogRef = this.dialog.open(HireVeteranPlayerDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(data => this.onHireNewPlayerClose(data));
  }

  handleFirePlayerClick() {
    if (this.selectedPlayerID != -1) {
      this.playerService.firePlayer(this.selectedPlayerID,
        () => {
            this.getPlayers();
            this.refreshed = !this.refreshed;
        });
    }
  }

  onHireNewPlayerClose(data: boolean | null): void {
    if (data != null || data) {
      this.getPlayers();
      this.refreshed = !this.refreshed;
    }
  }
}
