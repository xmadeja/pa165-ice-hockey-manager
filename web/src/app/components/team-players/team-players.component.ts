import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Player} from 'src/app/models/player';
import {PlayerService} from 'src/app/services/player.service';

@Component({
  selector: 'app-team-players',
  templateUrl: './team-players.component.html',
  styleUrls: ['./team-players.component.css']
})
export class TeamPlayersComponent implements OnInit {

  id: number = 0;

  players: Player[] = [];

  constructor(
    private playerService: PlayerService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.params.id);
    this.getPlayers();
  }

  getPlayers() {
    this.playerService.getPlayersForTeam(this.id)
      .subscribe(players => this.players = players);
  }

}
