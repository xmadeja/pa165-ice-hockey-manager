import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RecruitNewPlayerFormComponent} from './recruit-new-player-form.component';

describe('RecruitNewPlayerFormComponent', () => {
  let component: RecruitNewPlayerFormComponent;
  let fixture: ComponentFixture<RecruitNewPlayerFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecruitNewPlayerFormComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruitNewPlayerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
