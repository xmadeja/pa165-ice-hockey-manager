import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CreatePlayer} from 'src/app/models/create-player';
import {Team} from 'src/app/models/team';
import {PlayerService} from 'src/app/services/player.service';

@Component({
  selector: 'app-recruit-new-player-form',
  templateUrl: './recruit-new-player-form.component.html',
  styleUrls: ['./recruit-new-player-form.component.css']
})
export class RecruitNewPlayerFormComponent implements OnInit {

  @Output() formSubmitEvent: EventEmitter<any> = new EventEmitter();

  @Input() team!: Team;

  model: CreatePlayer = {name: ""};


  submitted = false;

  confirmed = false;

  constructor(
    private playerService: PlayerService
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.submitted = true;
    this.confirmSubmission();
  }

  resetForm(): void {
    this.model = {name: ""};
  }

  confirmSubmission(): void {
    this.confirmed = true;
    this.playerService.recruitPlayer(this.model, this.team);
    this.formSubmitEvent.emit(null);
  }
}
