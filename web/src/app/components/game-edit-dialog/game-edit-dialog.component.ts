import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Game } from 'src/app/models/game';
import { HireNewPlayerDialogComponent } from '../hire-new-player-dialog/hire-new-player-dialog.component';

@Component({
  selector: 'app-game-edit-dialog',
  templateUrl: './game-edit-dialog.component.html',
  styleUrls: ['./game-edit-dialog.component.css']
})
export class GameEditDialogComponent implements OnInit {

    game! : Game;

    constructor(
        private dialogRef: MatDialogRef<HireNewPlayerDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data: any
      ) {
          this.game = data.game;
    }

  
  ngOnInit(): void {
  }

  onFormSubmit(): void {
    this.dialogRef.close(true);
  }

}
