import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HireVeteranPlayerDialogComponent} from './hire-veteran-player-dialog.component';

describe('HireVeteranPlayerDialogComponent', () => {
  let component: HireVeteranPlayerDialogComponent;
  let fixture: ComponentFixture<HireVeteranPlayerDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HireVeteranPlayerDialogComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HireVeteranPlayerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
