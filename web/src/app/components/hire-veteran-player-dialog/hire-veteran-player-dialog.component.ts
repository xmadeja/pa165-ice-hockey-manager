import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Team} from 'src/app/models/team';
import {HireNewPlayerDialogComponent} from '../hire-new-player-dialog/hire-new-player-dialog.component';

@Component({
  selector: 'app-hire-veteran-player-dialog',
  templateUrl: './hire-veteran-player-dialog.component.html',
  styleUrls: ['./hire-veteran-player-dialog.component.css']
})
export class HireVeteranPlayerDialogComponent implements OnInit {

  team!: Team;

  constructor(
    private dialogRef: MatDialogRef<HireNewPlayerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.team = data.team;
  }

  ngOnInit(): void {
  }

  onFormSubmit(): void {
    this.dialogRef.close(true);
  }

}
