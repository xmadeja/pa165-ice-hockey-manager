import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CreateTeam} from 'src/app/models/create-team';
import {TeamService} from 'src/app/services/team.service';

@Component({
  selector: 'app-create-team-form',
  templateUrl: './create-team-form.component.html',
  styleUrls: ['./create-team-form.component.css']
})
export class CreateTeamFormComponent implements OnInit {

  @Output() formSubmitEvent: EventEmitter<any> = new EventEmitter();

  model: CreateTeam = {name: ""};

  submitted = false;

  confirmed = false;

  constructor(
    private teamService: TeamService
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.submitted = true;
    this.confirmSubmission();
    this.model = {name: ""};
  }

  resetForm(): void {
    this.model = {name: ""};
  }

  confirmSubmission(): void {
    this.confirmed = true;
    this.teamService.createTeam(this.model);
    this.formSubmitEvent.emit(null);
  }

}
