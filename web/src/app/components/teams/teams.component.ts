import {Component, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import { MatTable } from '@angular/material/table';

import {Team} from '../../models/team'
import {TeamService} from '../../services/team.service'

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  teams: Team[] = [];

  // @ViewChild(MatTable) table!: MatTable<any>;

  columnsToDisplay: string[] = ['name'];

  constructor(private teamService: TeamService) {
  }

  ngOnInit(): void {
    this.getTeams();
  }

  // ngOnChanges(changes: SimpleChanges) {
  //   this.table?.renderRows();
  // }

  getTeams() {
    this.teamService.getTeams()
      .subscribe(teams => this.teams = teams);
  }
}
