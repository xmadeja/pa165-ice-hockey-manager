import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {MatTable} from '@angular/material/table';
import {Player} from 'src/app/models/player';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.css']
})
export class PlayersComponent implements OnInit, OnChanges {

  @Input() players!: Player[];

  @Input() refreshed!: boolean;

  @Input() highlightSelected: boolean = false;

  @Output() selectRowEvent: EventEmitter<number> = new EventEmitter<number>();

  @ViewChild(MatTable) table!: MatTable<any>;

  columnsToDisplay = ['name'];

  selectedRowIndex: number = -1;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.table?.renderRows();
  }

  highlight(row: Player): void {
    if (this.selectedRowIndex == row.id) {
      this.selectedRowIndex = -1;
    } else {
      this.selectedRowIndex = row.id
    }
    this.selectRowEvent.emit(this.selectedRowIndex);
  }
}
