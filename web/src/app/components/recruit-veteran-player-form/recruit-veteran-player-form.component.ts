import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Player} from 'src/app/models/player';
import {RecruitVeteranPlayer} from 'src/app/models/recruit-veteran-player';
import {Team} from 'src/app/models/team';
import {PlayerService} from 'src/app/services/player.service';

@Component({
  selector: 'app-recruit-veteran-player-form',
  templateUrl: './recruit-veteran-player-form.component.html',
  styleUrls: ['./recruit-veteran-player-form.component.css']
})
export class RecruitVeteranPlayerFormComponent implements OnInit {

  @Output() formSubmitEvent: EventEmitter<any> = new EventEmitter();

  @Input() team!: Team;

  model!: RecruitVeteranPlayer;

  players: Player[] = [];

  submitted = false;

  confirmed = false;

  constructor(
    private playerService: PlayerService
  ) {
  }

  ngOnInit(): void {
    this.getPlayers();
    this.resetForm();
  }

  onSubmit(): void {
    this.submitted = true;
    this.confirmSubmission();
  }

  getPlayers(): void {
    this.playerService.getFreePlayers()
      .subscribe(players => this.players = players);
  }

  resetForm(): void {
    this.model = {player: null, team: this.team};
  }

  confirmSubmission(): void {
    this.confirmed = true;
    this.playerService.recruitVeteranPlayer(this.model.player!, this.team);
    this.formSubmitEvent.emit(null);
  }
}
