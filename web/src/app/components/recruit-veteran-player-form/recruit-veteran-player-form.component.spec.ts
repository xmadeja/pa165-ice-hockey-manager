import {ComponentFixture, TestBed} from '@angular/core/testing';

import {RecruitVeteranPlayerFormComponent} from './recruit-veteran-player-form.component';

describe('RecruitVeteranPlayerFormComponent', () => {
  let component: RecruitVeteranPlayerFormComponent;
  let fixture: ComponentFixture<RecruitVeteranPlayerFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecruitVeteranPlayerFormComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecruitVeteranPlayerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
