import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CreateGame} from 'src/app/models/create-game';
import {Team} from 'src/app/models/team';
import {GameService} from 'src/app/services/game.service';
import {TeamService} from 'src/app/services/team.service';

@Component({
  selector: 'app-create-game-form',
  templateUrl: './create-game-form.component.html',
  styleUrls: ['./create-game-form.component.css']
})
export class CreateGameFormComponent implements OnInit {

  @Output() formSubmitEvent: EventEmitter<any> = new EventEmitter();

  model: CreateGame = {gameDateTime: null, homeTeam: null, awayTeam: null};

  teams: Team[] = [];

  submitted = false;

  confirmed = false;

  constructor(
    private teamService: TeamService,
    private gameService: GameService
  ) {
  }

  ngOnInit(): void {
    this.getTeams();
  }

  getTeams(): void {
    this.teamService.getTeams()
      .subscribe(teams => this.teams = teams);
  }

  onSubmit(): void {
    this.submitted = true;
    this.confirmSubmission();
    this.resetForm();
  }

  resetForm(): void {
    this.model = {gameDateTime: null, homeTeam: null, awayTeam: null};
  }

  confirmSubmission(): void {
    this.confirmed = true;
    this.gameService.createGame(this.model);
    this.formSubmitEvent.emit(null);
  }
}
