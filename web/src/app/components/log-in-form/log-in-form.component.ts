import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserAuthRequest} from 'src/app/models/user-auth-request';
import {AuthServiceService} from 'src/app/services/auth-service.service';

@Component({
  selector: 'app-log-in-form',
  templateUrl: './log-in-form.component.html',
  styleUrls: ['./log-in-form.component.css']
})
export class LogInFormComponent implements OnInit {

  @Output() formSubmitEvent: EventEmitter<any> = new EventEmitter();

  model: UserAuthRequest = {username: "", password: ""};

  submitted = false;

  confirmed = false;

  constructor(
    private authService: AuthServiceService
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.submitted = true;
    this.confirmSubmission();
  }

  resetForm(): void {
    this.model = {username: "", password: ""};
  }

  confirmSubmission(): void {
    this.confirmed = true;
    this.authService.authenticate(this.model, () => this.formSubmitEvent.emit(null));
  }
}
