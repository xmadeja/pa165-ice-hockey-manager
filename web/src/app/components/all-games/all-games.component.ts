import {Component, OnInit} from '@angular/core';
import {Game} from 'src/app/models/game';
import {GameService} from 'src/app/services/game.service';

@Component({
  selector: 'app-all-games',
  templateUrl: './all-games.component.html',
  styleUrls: ['./all-games.component.css']
})
export class AllGamesComponent implements OnInit {

  games: Array<Game> = [];

  constructor(private gameService: GameService) {
  }

  ngOnInit(): void {
    this.getGames();
  }

  getGames() {
    this.gameService.getGames()
      .subscribe(games => this.games = games);
  }

  handleRefreshEvent() {
    this.getGames();
  }
}
