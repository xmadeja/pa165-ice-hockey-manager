import {Component, OnInit} from '@angular/core';
import {Player} from 'src/app/models/player';
import {PlayerService} from 'src/app/services/player.service';

@Component({
  selector: 'app-allplayers',
  templateUrl: './allplayers.component.html',
  styleUrls: ['./allplayers.component.css']
})
export class AllplayersComponent implements OnInit {

  players: Player[] = [];

  constructor(private playerService: PlayerService) {
  }

  ngOnInit(): void {
    this.getPlayers();
  }

  getPlayers() {
    this.playerService.getPlayers()
      .subscribe(players => this.players = players);
  }
}
