import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Game} from 'src/app/models/game';
import {GameService} from 'src/app/services/game.service';

@Component({
  selector: 'app-update-game-score-form',
  templateUrl: './update-game-score-form.component.html',
  styleUrls: ['./update-game-score-form.component.css']
})
export class UpdateGameScoreFormComponent implements OnInit {

  @Output() formSubmitEvent: EventEmitter<any> = new EventEmitter();

  @Input() model!: Game;

  modelCopy!: Game;

  submitted = false;

  confirmed = false;

  constructor(
    private gameService: GameService
  ) {
  }

  ngOnInit(): void {
    this.copyModel();
  }

  copyModel(): void {
    this.modelCopy = {...this.model};
  }

  onSubmit(): void {
    this.submitted = true;
    this.confirmSubmission();
  }

  resetForm(): void {
    this.modelCopy.homeTeamScore = this.model.homeTeamScore;
    this.modelCopy.awayTeamScore = this.model.awayTeamScore;
  }

  confirmSubmission(): void {
    this.confirmed = true;
    this.gameService.updateGameScore(this.modelCopy);
    this.formSubmitEvent.emit(null);
  }
}
