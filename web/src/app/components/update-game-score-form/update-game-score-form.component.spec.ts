import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UpdateGameScoreFormComponent} from './update-game-score-form.component';

describe('UpdateGameScoreFormComponent', () => {
  let component: UpdateGameScoreFormComponent;
  let fixture: ComponentFixture<UpdateGameScoreFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateGameScoreFormComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGameScoreFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
