import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AppComponent } from 'src/app/app.component';
import {Game} from 'src/app/models/game';
import { GameEditDialogComponent } from '../game-edit-dialog/game-edit-dialog.component';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  @Input() games!: Array<Game>;

  @Output() reloadEvent: EventEmitter<any> = new EventEmitter();

  columnsToDisplay: string[] = [
    "name", "homeTeam", "awayTeam", "score"
  ];

  constructor(private dialog: MatDialog,
    private appComp: AppComponent) {
  }

  ngOnInit(): void {
  }

  triggerRefreshEvent(): void {
    this.reloadEvent.emit(null);
  }

  isLeagueManager() : boolean {
      return this.appComp.isAuthenticated;
  }

  handleEditClick(game: Game) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {
      game: game
    };
    const dialogRef = this.dialog.open(GameEditDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(() => this.triggerRefreshEvent());
  }

}
