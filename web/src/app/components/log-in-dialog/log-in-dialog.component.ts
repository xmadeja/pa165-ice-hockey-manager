import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HireNewPlayerDialogComponent} from '../hire-new-player-dialog/hire-new-player-dialog.component';

@Component({
  selector: 'app-log-in-dialog',
  templateUrl: './log-in-dialog.component.html',
  styleUrls: ['./log-in-dialog.component.css']
})
export class LogInDialogComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<HireNewPlayerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
  }

  ngOnInit(): void {
  }

  onFormSubmit(): void {
    this.dialogRef.close(true);
  }

}
