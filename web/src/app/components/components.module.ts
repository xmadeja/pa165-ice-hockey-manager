import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TeamsComponent} from './teams/teams.component';
import {GamesComponent} from './games/games.component';
import {PlayersComponent} from './players/players.component';
import {AllplayersComponent} from './allplayers/allplayers.component';
import {TeamPlayersComponent} from './team-players/team-players.component';
import {AllGamesComponent} from './all-games/all-games.component';
import {DeleteGameButtonComponent} from './delete-game-button/delete-game-button.component';
import {CreateGameFormComponent} from './create-game-form/create-game-form.component';
import {UpdateGameScoreFormComponent} from './update-game-score-form/update-game-score-form.component';
import {CreateTeamFormComponent} from './create-team-form/create-team-form.component';
import {RecruitNewPlayerFormComponent} from './recruit-new-player-form/recruit-new-player-form.component';
import {RecruitVeteranPlayerFormComponent} from './recruit-veteran-player-form/recruit-veteran-player-form.component';
import {UpdateGameWinnerFormComponent} from './update-game-winner-form/update-game-winner-form.component';
import {TeamManagementDashboardComponent} from './team-management-dashboard/team-management-dashboard.component';
import {HireNewPlayerDialogComponent} from './hire-new-player-dialog/hire-new-player-dialog.component';
import {HireVeteranPlayerDialogComponent} from './hire-veteran-player-dialog/hire-veteran-player-dialog.component';
import {LogInFormComponent} from './log-in-form/log-in-form.component';
import {LogInDialogComponent} from './log-in-dialog/log-in-dialog.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { GameEditDialogComponent } from './game-edit-dialog/game-edit-dialog.component';


@NgModule({
  declarations: [
    TeamsComponent,
    GamesComponent,
    PlayersComponent,
    AllplayersComponent,
    TeamPlayersComponent,
    AllGamesComponent,
    DeleteGameButtonComponent,
    CreateGameFormComponent,
    UpdateGameScoreFormComponent,
    CreateTeamFormComponent,
    RecruitNewPlayerFormComponent,
    RecruitVeteranPlayerFormComponent,
    UpdateGameWinnerFormComponent,
    TeamManagementDashboardComponent,
    HireNewPlayerDialogComponent,
    HireVeteranPlayerDialogComponent,
    LogInFormComponent,
    LogInDialogComponent,
    WelcomeComponent,
    GameEditDialogComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TeamsComponent,
    GamesComponent,
    PlayersComponent,
    AllplayersComponent,
    TeamPlayersComponent
  ]
})
export class ComponentsModule {
}
