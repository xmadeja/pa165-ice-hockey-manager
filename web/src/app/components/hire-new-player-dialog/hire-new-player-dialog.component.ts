import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Team} from 'src/app/models/team';

@Component({
  selector: 'app-hire-new-player-dialog',
  templateUrl: './hire-new-player-dialog.component.html',
  styleUrls: ['./hire-new-player-dialog.component.css']
})
export class HireNewPlayerDialogComponent implements OnInit {

  team!: Team;

  constructor(
    private dialogRef: MatDialogRef<HireNewPlayerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) data: any
  ) {
    this.team = data.team;
  }

  ngOnInit(): void {
  }

  onFormSubmit(): void {
    this.dialogRef.close(true);
  }
}
