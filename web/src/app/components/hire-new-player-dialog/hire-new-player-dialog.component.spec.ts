import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HireNewPlayerDialogComponent} from './hire-new-player-dialog.component';

describe('HireNewPlayerDialogComponent', () => {
  let component: HireNewPlayerDialogComponent;
  let fixture: ComponentFixture<HireNewPlayerDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HireNewPlayerDialogComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HireNewPlayerDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
