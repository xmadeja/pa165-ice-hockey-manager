import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GameService} from 'src/app/services/game.service';

@Component({
  selector: 'app-delete-game-button',
  templateUrl: './delete-game-button.component.html',
  styleUrls: ['./delete-game-button.component.css']
})

export class DeleteGameButtonComponent implements OnInit {

  @Input() id!: number;

  @Output() onDelete: EventEmitter<number> = new EventEmitter();

  constructor(private gameService: GameService) {
  }

  ngOnInit(): void {
  }

  onDeleteClick(): void {
    this.gameService.deleteGame(this.id, () => this.onDelete.emit(this.id));
  }
}
