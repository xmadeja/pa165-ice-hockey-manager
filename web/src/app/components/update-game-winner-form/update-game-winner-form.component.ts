import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Game} from 'src/app/models/game';
import {Team} from 'src/app/models/team';
import {UpdateGameWinner} from 'src/app/models/update-game-winner';
import {GameService} from 'src/app/services/game.service';

@Component({
  selector: 'app-update-game-winner-form',
  templateUrl: './update-game-winner-form.component.html',
  styleUrls: ['./update-game-winner-form.component.css']
})
export class UpdateGameWinnerFormComponent implements OnInit {

  @Output() formSubmitEvent: EventEmitter<any> = new EventEmitter();

  @Input() game!: Game;

  model!: UpdateGameWinner;

  teams!: Team[];

  submitted = false;

  confirmed = false;

  constructor(
    private gameService: GameService
  ) {
  }

  ngOnInit(): void {
    this.getTeams();
    this.initModel();
    this.resetForm();
  }

  getTeams(): void {
    this.teams = [this.game.homeTeam, this.game.awayTeam];
  }

  initModel(): void {
    this.model = {id: this.game.id, winner: null};
  }

  onSubmit(): void {
    this.submitted = true;
  }

  resetForm(): void {
    this.model.winner = null;
  }

  confirmSubmission(): void {
    this.confirmed = true;
    this.gameService.updateGameWinner(this.model);
    this.formSubmitEvent.emit(null);
  }

}
