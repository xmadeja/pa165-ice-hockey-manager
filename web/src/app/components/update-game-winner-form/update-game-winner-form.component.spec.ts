import {ComponentFixture, TestBed} from '@angular/core/testing';

import {UpdateGameWinnerFormComponent} from './update-game-winner-form.component';

describe('UpdateGameWinnerFormComponent', () => {
  let component: UpdateGameWinnerFormComponent;
  let fixture: ComponentFixture<UpdateGameWinnerFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UpdateGameWinnerFormComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateGameWinnerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
