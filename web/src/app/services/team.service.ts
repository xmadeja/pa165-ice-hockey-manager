import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators'; 

import {EMPTY, Observable, of} from 'rxjs';
import {AccessEnum} from '../models/access-enum';
import {AssociatedTeam} from '../models/associated-team';
import {CreateTeam} from '../models/create-team';

import {Team} from '../models/team'
import { AuthServiceService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  getAllUrl: string = 'http://localhost:8080/pa165/api/public/team/all';
  createUrl: string = 'http://localhost:8080/pa165/api/manage/league/team/create';

  readonly GET_MANAGED_TEAM: string = 'http://localhost:8080/pa165/api/manage/team/team/get';

  constructor(
    private http: HttpClient,
    private auth: AuthServiceService
    ) {
  }

  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(this.getAllUrl);
  }

  createTeam(team: CreateTeam): void {
    const headerVal = this.auth.getBearerHeader();
    this.http.post<Team>(this.createUrl, team, {
        headers: {Authorization : headerVal}
      })
      .subscribe();
  }

  getAssociatedTeam(): Observable<Team> {
    const headerVal = this.auth.getBearerHeader();
    return this.http.get<Team>(
        this.GET_MANAGED_TEAM,
        {
          headers: {Authorization : headerVal}
        }
      );
  }
}

