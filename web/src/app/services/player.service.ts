import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {CreatePlayer} from '../models/create-player';
import {Player} from '../models/player';
import {Team} from '../models/team';
import {RecruitNewPlayer} from '../models/recruit-new-player';
import {RecruitVeteran} from '../models/recruit-veteran';
import { AuthServiceService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  getAllFreeUrl: string = 'http://localhost:8080/pa165/api/public/player/free';
  getAllUrl: string = 'http://localhost:8080/pa165/api/public/player/all';
  getForTeamUrl: string = 'http://localhost:8080/pa165/api/public/player/team/'
  createNewUrl: string = 'http://localhost:8080/pa165/api/manage/team/player/recruit';
  recruitUrl: string = 'http://localhost:8080/pa165/api/manage/team/player/transfer';
  fireUrl: string = 'http://localhost:8080/pa165/api/manage/team/player/fire/';

  constructor(private http: HttpClient, private auth : AuthServiceService) {

  }

  getPlayers(): Observable<Player[]> {
    return this.http.get<Player[]>(this.getAllUrl);
  }

  getFreePlayers(): Observable<Player[]> {
    return this.http.get<Player[]>(this.getAllFreeUrl);
  }

  getPlayersForTeam(id: number): Observable<Player[]> {
    return this.http.get<Player[]>(this.getForTeamUrl + id);
  }

  recruitPlayer(player: CreatePlayer, team: Team): void {
    const headerVal = this.auth.getBearerHeader();
    let newPlayer: RecruitNewPlayer = {name: player.name, teamId: team.id};
    this.http.post<RecruitNewPlayer>(this.createNewUrl, newPlayer, {headers : 
        {Authorization : headerVal}
    }).subscribe();
  }

  recruitVeteranPlayer(player: Player, team: Team): void {
    const headerVal = this.auth.getBearerHeader();
    let newPlayer: RecruitVeteran = {playerId: player.id, teamId: team.id};
    this.http.post<RecruitVeteran>(this.recruitUrl, newPlayer, {headers : 
        {Authorization : headerVal}
    }).subscribe();
  }

  firePlayer(id: number, callback : Function): void {
    const headerVal = this.auth.getBearerHeader();
    this.http.delete(this.fireUrl + id, {headers : 
        {Authorization : headerVal}
    }).subscribe(() => callback());
  }
}
