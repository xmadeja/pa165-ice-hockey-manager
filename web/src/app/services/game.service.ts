import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {CreateGame} from '../models/create-game';
import {CreateGameShort} from '../models/create-game-short';
import {Game} from '../models/game';
import {UpdateGameWinner} from '../models/update-game-winner';
import {UpdateGameWinnerShort} from '../models/update-game-winner-short';
import {GameShort} from '../models/game-short';
import { AuthServiceService } from './auth-service.service';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  getAllUrl: string = 'http://localhost:8080/pa165/api/public/game/all';
  createUrl: string = 'http://localhost:8080/pa165/api/manage/league/game/create';
  updateWinnerUrl: string = 'http://localhost:8080/pa165/api/manage/league/game/setWinner';
  deleteUrl: string = 'http://localhost:8080/pa165/api/manage/league/game/delete/';
  updateUrl: string = 'http://localhost:8080/pa165/api/manage/league/game/update';

  constructor(private http: HttpClient, private auth: AuthServiceService) {
  }

  getGames(): Observable<Game[]> {
    return this.http.get<Game[]>(this.getAllUrl);
  }

  deleteGame(id: number, callback: Function) {
    const headerVal = this.auth.getBearerHeader();
    this.http.delete(this.deleteUrl + id, {
        headers: {Authorization : headerVal}
      }).subscribe(e => callback());
  }

  createGame(game: CreateGame): void {
    const headerVal = this.auth.getBearerHeader();
    let newGame: CreateGameShort = {
      gameDateTime: game.gameDateTime,
      homeTeamId: game.homeTeam?.id, awayTeamId: game.awayTeam?.id,
      homeTeamScore: 0, awayTeamScore: 0
    };
    this.http.post<GameShort>(this.createUrl, newGame, {
        headers: {Authorization : headerVal}
      }).subscribe();
  }

  updateGameScore(game: Game): void {
    const headerVal = this.auth.getBearerHeader();
    let updateGame: GameShort = {
      id: game.id, gameDateTime: game.gameDateTime,
      homeTeamId: game.homeTeam.id, awayTeamId: game.awayTeam.id,
      winnerId: game.winner?.id, awayTeamScore: game.awayTeamScore, homeTeamScore: game.homeTeamScore
    };
    this.http.put<GameShort>(this.updateUrl, updateGame, {
        headers: {Authorization : headerVal}
      }).subscribe();
  }

  updateGameWinner(game: UpdateGameWinner): void {
    const headerVal = this.auth.getBearerHeader();
    let updateGame: UpdateGameWinnerShort = {
      id: game.id, winnerId: game.winner?.id
    };
    this.http.put<GameShort>(this.updateWinnerUrl, updateGame, {
        headers: {Authorization : headerVal}
      }).subscribe();
  }
}
