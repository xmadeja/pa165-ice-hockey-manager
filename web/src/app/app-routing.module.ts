import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AllGamesComponent} from './components/all-games/all-games.component';
import {AllplayersComponent} from './components/allplayers/allplayers.component';
import {CreateGameFormComponent} from './components/create-game-form/create-game-form.component';
import {CreateTeamFormComponent} from './components/create-team-form/create-team-form.component';
import {TeamManagementDashboardComponent} from './components/team-management-dashboard/team-management-dashboard.component';
import {TeamPlayersComponent} from './components/team-players/team-players.component';
import {TeamsComponent} from './components/teams/teams.component';
import { WelcomeComponent } from './components/welcome/welcome.component';


const routes: Routes = [
  {path: 'teams', component: TeamsComponent},
  {path: 'games', component: AllGamesComponent},
  {path: 'players', component: AllplayersComponent},
  {path: 'team/players/:id', component: TeamPlayersComponent},
  {path: 'game/create', component: CreateGameFormComponent},
  {path: 'team/create', component: CreateTeamFormComponent},
  {path: 'team/manage', component: TeamManagementDashboardComponent},
  {path: 'welcome', component: WelcomeComponent},
  {path: '', redirectTo: 'welcome', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
