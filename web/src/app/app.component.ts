import {Component} from '@angular/core';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {LogInDialogComponent} from './components/log-in-dialog/log-in-dialog.component';
import {AuthServiceService} from './services/auth-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Ice Hockey Manager';

  isAuthenticated: boolean = false;
  isTeamManager: boolean = false;
  isLeagueManager: boolean = false;

  constructor(
    private dialog: MatDialog,
    private authService: AuthServiceService
  ) {
  }

  ngOnInit(): void {
    this.refreshAuth();
  }

  refreshAuth(): void {
    this.authService.isAuthenticated().subscribe(e => this.isAuthenticated = e);
    this.authService.isTeamManager().subscribe(e => this.isTeamManager = e);
    this.authService.isLeagueManager().subscribe(e => this.isLeagueManager = e);
  }

  handleLogInClick()  {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = {};
    const dialogRef = this.dialog.open(LogInDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(data => this.onLogInClose(data));
  }

  onLogInClose(data: boolean | null): void {
    if (data != null || data) {
      this.refreshAuth();
    }
  }

  handleLogOutClick() {
    this.authService.logOut()
      .subscribe((success: boolean) => {
        if (success) {
          this.refreshAuth();
        }
      });
  }
}
