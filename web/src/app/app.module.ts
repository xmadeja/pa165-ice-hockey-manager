import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {GamesComponent} from './components/games/games.component';
import {TeamsComponent} from './components/teams/teams.component';
import {PlayersComponent} from './components/players/players.component';
import {AllplayersComponent} from './components/allplayers/allplayers.component';
import {TeamPlayersComponent} from './components/team-players/team-players.component';
import {AllGamesComponent} from './components/all-games/all-games.component';
import {DeleteGameButtonComponent} from './components/delete-game-button/delete-game-button.component';
import {CreateGameFormComponent} from './components/create-game-form/create-game-form.component';
import {UpdateGameScoreFormComponent} from './components/update-game-score-form/update-game-score-form.component';
import {CreateTeamFormComponent} from './components/create-team-form/create-team-form.component';
import {RecruitNewPlayerFormComponent} from './components/recruit-new-player-form/recruit-new-player-form.component';
import {RecruitVeteranPlayerFormComponent} from './components/recruit-veteran-player-form/recruit-veteran-player-form.component';
import {UpdateGameWinnerFormComponent} from './components/update-game-winner-form/update-game-winner-form.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TeamManagementDashboardComponent} from './components/team-management-dashboard/team-management-dashboard.component';
import {HireNewPlayerDialogComponent} from './components/hire-new-player-dialog/hire-new-player-dialog.component';
import {HireVeteranPlayerDialogComponent} from './components/hire-veteran-player-dialog/hire-veteran-player-dialog.component';
import {LogInFormComponent} from './components/log-in-form/log-in-form.component';
import {LogInDialogComponent} from './components/log-in-dialog/log-in-dialog.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { GameEditDialogComponent } from './components/game-edit-dialog/game-edit-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    GamesComponent,
    TeamsComponent,
    PlayersComponent,
    AllplayersComponent,
    TeamPlayersComponent,
    AllGamesComponent,
    DeleteGameButtonComponent,
    CreateGameFormComponent,
    UpdateGameScoreFormComponent,
    CreateTeamFormComponent,
    RecruitNewPlayerFormComponent,
    RecruitVeteranPlayerFormComponent,
    UpdateGameWinnerFormComponent,
    TeamManagementDashboardComponent,
    HireNewPlayerDialogComponent,
    HireVeteranPlayerDialogComponent,
    LogInFormComponent,
    LogInDialogComponent,
    WelcomeComponent,
    GameEditDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatTableModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatDialogModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
