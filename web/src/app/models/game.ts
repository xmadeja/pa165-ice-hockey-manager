import {Team} from "./team";

export interface Game {
  id: number;

  gameDateTime: string;

  homeTeam: Team;
  awayTeam: Team;
  winner: Team;

  homeTeamScore: number;
  awayTeamScore: number;
}
