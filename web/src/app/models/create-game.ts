import {Team} from "./team";

export interface CreateGame {
  gameDateTime: string | null;

  homeTeam: Team | null;
  awayTeam: Team | null;
}
