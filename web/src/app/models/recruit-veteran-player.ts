import {Player} from "./player";
import {Team} from "./team";

export interface RecruitVeteranPlayer {
  player: Player | null;
  team: Team;
}
