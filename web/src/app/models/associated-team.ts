import {AccessEnum} from "./access-enum";
import {Team} from "./team";

export interface AssociatedTeam {
  stats: AccessEnum;
  team: Team | null;
}
