export interface RecruitNewPlayer {
  name: string;
  teamId: number;
}
