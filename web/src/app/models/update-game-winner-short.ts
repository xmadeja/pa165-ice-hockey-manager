export interface UpdateGameWinnerShort {
  id: number;
  winnerId: number | undefined;
}
