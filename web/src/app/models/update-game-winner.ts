import {Team} from "./team";

export interface UpdateGameWinner {
  id: number;
  winner: Team | null;
}
