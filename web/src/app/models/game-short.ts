export interface GameShort {
  id: number;

  gameDateTime: string;

  homeTeamId: number;
  awayTeamId: number;
  winnerId: number;

  homeTeamScore: number;
  awayTeamScore: number;
}
