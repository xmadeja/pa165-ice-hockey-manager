export interface CreateGameShort {
  gameDateTime: string | null;

  homeTeamId: number | undefined;
  awayTeamId: number | undefined;
  homeTeamScore: number | undefined;
  awayTeamScore: number | undefined;
}
