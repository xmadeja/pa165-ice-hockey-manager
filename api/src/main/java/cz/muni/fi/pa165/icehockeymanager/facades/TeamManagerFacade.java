package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.dto.PlayerCreateDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerTransferDto;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownPlayerException;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownTeamException;

import java.util.Collection;

/**
 * Facade with functionality for the team manager,
 * such as managing players within team.
 */
public interface TeamManagerFacade {

    /**
     * Creates new player and adds it to team given by teamId
     *
     * @param playerCreateDTO DTO object for creating new player
     */
    void recruitNewPlayer(PlayerCreateDto playerCreateDTO);

    /**
     * Adds player with playerId to team with teamId
     *
     * @param playerTransferDTO DTO for recruiting veteran player
     */
    void recruitPlayer(PlayerTransferDto playerTransferDTO);

    /**
     * Retrieve all players without team
     *
     * @return Collection of free players
     */
    Collection<PlayerDto> getFreePlayers();

    /**
     * Delete player with playerId from team with teamId
     *
     * @param playerId Identifier of player
     * @throws UnknownPlayerException If playerId does not match any player
     * @throws UnknownTeamException   If teamId does not match any team
     */
    void firePlayer(long playerId);
}
