package cz.muni.fi.pa165.icehockeymanager.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Getter
@Setter
public class TeamCreateDto {

    @NotBlank
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamCreateDto teamDTO = (TeamCreateDto) o;
        return getName().equals(teamDTO.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

}
