package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.dto.GameCreateDto;
import cz.muni.fi.pa165.icehockeymanager.dto.GameUpdateScoreDto;
import cz.muni.fi.pa165.icehockeymanager.dto.GameWinnerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.TeamCreateDto;

/**
 * Facade with functionality for the league manager,
 * such as managing games and teams in the league.
 */
public interface LeagueManagerFacade {

    /**
     * Creates and plans a new game.
     *
     * @param gameCreateDTO DTO object for when game is created (without id)
     */
    void createGame(GameCreateDto gameCreateDTO);

    /**
     * Removes game from the schedule.
     *
     * @param gameId ID of the game to be removed.
     */
    void dropGame(long gameId);

    /**
     * Updates the game score and also the winner if one is determined by the new score.
     *
     * @param game DTO for updating game
     */
    void updateGameScore(GameUpdateScoreDto game);

    /**
     * Sets the winner of a game.
     *
     * @param game DTO for setting game winner
     */
    void setGameWinner(GameWinnerDto game);

    /**
     * Add new team to the league.
     *
     * @param teamCreateDTO DTO of the new team to be created.
     */
    void createNewTeam(TeamCreateDto teamCreateDTO);
}
