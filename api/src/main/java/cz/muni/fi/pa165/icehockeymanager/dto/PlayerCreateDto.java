package cz.muni.fi.pa165.icehockeymanager.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Getter
@Setter
public final class PlayerCreateDto {

    @NotBlank
    private String name;

    private long teamId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerCreateDto playerDTO = (PlayerCreateDto) o;
        return getName().equals(playerDTO.getName()) && getTeamId() == playerDTO.getTeamId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
