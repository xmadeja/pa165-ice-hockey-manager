package cz.muni.fi.pa165.icehockeymanager.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
public class GameUpdateScoreDto {

    @NotNull
    private Long id;

    @PositiveOrZero
    private int homeTeamScore;

    @PositiveOrZero
    private int awayTeamScore;
}
