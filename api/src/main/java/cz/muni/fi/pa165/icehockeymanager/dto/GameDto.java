package cz.muni.fi.pa165.icehockeymanager.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.ZonedDateTime;
import java.util.Objects;

@Getter
@Setter
public class GameDto {

    @NotNull
    private Long id;

    @NotNull
    private TeamDto homeTeam;

    @NotNull
    private TeamDto awayTeam;

    private TeamDto winner;

    @PositiveOrZero
    private int homeTeamScore;

    @PositiveOrZero
    private int awayTeamScore;

    @NotNull
    private ZonedDateTime gameDateTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameDto gameDTO = (GameDto) o;
        return getHomeTeamScore() == gameDTO.getHomeTeamScore() &&
                getAwayTeamScore() == gameDTO.getAwayTeamScore() &&
                getId().equals(gameDTO.getId()) &&
                getHomeTeam().equals(gameDTO.getHomeTeam()) &&
                getAwayTeam().equals(gameDTO.getAwayTeam()) &&
                Objects.equals(getWinner(), gameDTO.getWinner()) &&
                getGameDateTime().equals(gameDTO.getGameDateTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getHomeTeam(), getAwayTeam(), getHomeTeamScore(), getAwayTeamScore(), getGameDateTime());
    }
}
