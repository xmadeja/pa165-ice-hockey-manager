package cz.muni.fi.pa165.icehockeymanager.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
public final class PlayerTransferDto {

    private long playerId;

    private long teamId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerTransferDto playerDTO = (PlayerTransferDto) o;
        return getPlayerId() == playerDTO.getPlayerId() && getTeamId() == playerDTO.getTeamId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPlayerId());
    }
}
