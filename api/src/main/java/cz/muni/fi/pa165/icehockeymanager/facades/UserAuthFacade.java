package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.dto.TeamDto;
import cz.muni.fi.pa165.icehockeymanager.dto.UserDto;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UsernameTakenException;

import java.util.Optional;

/**
 * Facade facilitating user authentication
 * and authorization.
 */
public interface UserAuthFacade {
    /**
     * Authenticate user
     *
     * @param username username
     * @param password plaintext password
     * @return true if the password is correct, else false
     */
    Optional<UserDto> authenticateUser(String username, String password);

    /**
     * Create new League Manager account
     *
     * @param username username
     * @param password plaintext password
     * @return User if exists
     * @throws UsernameTakenException if username is already taken by another user
     */
    UserDto createLeagueManager(String username, String password);

    /**
     * Create new Team Manager account
     *
     * @param username username
     * @param password plaintext password
     * @return User if exists
     * @throws UsernameTakenException if username is already taken by another user
     */
    UserDto createTeamManager(String username, String password, int teamId);

    /**
     * Find team associated with the manager
     *
     * @param username username
     * @return associated team
     */
    Optional<TeamDto> fetchTeamForManager(String username);

    /**
     * Find user by username
     *
     * @param username username
     * @return empty optional if user was not found
     */
    Optional<UserDto> fetchUser(String username);
}
