package cz.muni.fi.pa165.icehockeymanager.exceptions;

public class UnknownUserException extends RuntimeException {

    public UnknownUserException(String message) {
        super(message);
    }

    public UnknownUserException() {
    }
}
