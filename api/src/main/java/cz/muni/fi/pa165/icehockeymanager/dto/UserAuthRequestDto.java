package cz.muni.fi.pa165.icehockeymanager.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class UserAuthRequestDto {

    @NotBlank
    String username;

    @NotBlank
    String password;
}
