package cz.muni.fi.pa165.icehockeymanager.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.ZonedDateTime;
import java.util.Objects;

@Getter
@Setter
public class GameCreateDto {
    @NotNull
    private Long homeTeamId;

    @NotNull
    private Long awayTeamId;

    private Long winnerId;

    @PositiveOrZero
    private int homeTeamScore;

    @PositiveOrZero
    private int awayTeamScore;

    private ZonedDateTime gameDateTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameCreateDto gameDTO = (GameCreateDto) o;
        return getHomeTeamScore() == gameDTO.getHomeTeamScore() &&
                getAwayTeamScore() == gameDTO.getAwayTeamScore() &&
                getHomeTeamId().equals(gameDTO.getHomeTeamId()) &&
                getAwayTeamId().equals(gameDTO.getAwayTeamId()) &&
                Objects.equals(getWinnerId(), gameDTO.getWinnerId()) &&
                getGameDateTime().equals(gameDTO.getGameDateTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHomeTeamId(), getAwayTeamId(), getHomeTeamScore(), getAwayTeamScore(), getGameDateTime());
    }
}
