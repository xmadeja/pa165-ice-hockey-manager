package cz.muni.fi.pa165.icehockeymanager.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GameWinnerDto {
    @NotNull
    private Long id;

    @NotNull
    private Long teamId;
}
