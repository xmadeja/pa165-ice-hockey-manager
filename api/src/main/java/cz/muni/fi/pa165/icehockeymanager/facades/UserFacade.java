package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.dto.GameDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.TeamDto;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownTeamException;

import java.util.Collection;
import java.util.Optional;

/**
 * Facade containing functionality for viewing data.
 * Contains functionality available to any user.
 */
public interface UserFacade {

    /**
     * Retrieve collection of games for the given team.
     *
     * @param id Identifier of the team
     * @return DTO objects for matching games
     * @throws UnknownTeamException If id does not match any team
     */
    Collection<GameDto> getGamesForTeam(long id);

    /**
     * Retrieve all games in the league.
     *
     * @return DTO objects for all found games
     */
    Collection<GameDto> getGamesForLeague();

    /**
     * Retrieve player based on a unique idetifier.
     *
     * @param id Unique identifier of the player
     * @return Empty optional iff a player was not found, otherwise containing player
     */
    Optional<PlayerDto> findPlayer(long id);

    /**
     * Get all players for a given team.
     *
     * @param id Unique indentifier of a team
     * @return Collection of all players in the team
     * @throws UnknownTeamException If id does not match any team
     */
    Collection<PlayerDto> getPlayersInTeam(long id);

    /**
     * Get all players.
     *
     * @return Collection of all players in the league
     */
    Collection<PlayerDto> getAllPlayers();

    /**
     * Retrieve all teams in a league.
     *
     * @return Collection of teams
     */
    Collection<TeamDto> getTeamsInLeague();
}
