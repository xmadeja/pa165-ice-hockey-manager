package cz.muni.fi.pa165.icehockeymanager.security;

public enum Roles {
    LEAGUE_MANAGER,
    TEAM_MANAGER;

    @Override
    public String toString() {
        return this.name().toUpperCase();
    }
}
