package cz.muni.fi.pa165.icehockeymanager.rest.controller;

import cz.muni.fi.pa165.icehockeymanager.dto.PlayerCreateDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerTransferDto;
import cz.muni.fi.pa165.icehockeymanager.dto.TeamDto;
import cz.muni.fi.pa165.icehockeymanager.facades.TeamManagerFacade;
import cz.muni.fi.pa165.icehockeymanager.facades.UserAuthFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/pa165/api/manage/team")
public class TeamManagerController {

    private final TeamManagerFacade teamManagerFacade;
    private final UserAuthFacade userAuthFacade;

    @Autowired
    public TeamManagerController(
            TeamManagerFacade teamManagerFacade,
            UserAuthFacade userAuthFacade) {
        this.teamManagerFacade = teamManagerFacade;
        this.userAuthFacade = userAuthFacade;
    }

    @GetMapping("/team/get")
    public final TeamDto getManagedTeam(Authentication authentication) {
        var username = authentication.getName();
        return userAuthFacade.fetchTeamForManager(username).orElse(null);
    }

    @DeleteMapping("/player/fire/{id}")
    public final void firePlayer(@PathVariable Long id) {
        teamManagerFacade.firePlayer(id);
    }

    @PostMapping("/player/recruit")
    public final void createPlayer(@Valid @RequestBody PlayerCreateDto player) {
        teamManagerFacade.recruitNewPlayer(player);
    }

    @PostMapping("/player/transfer")
    public final void recruitPlayer(@Valid @RequestBody PlayerTransferDto player) {
        teamManagerFacade.recruitPlayer(player);
    }
}
