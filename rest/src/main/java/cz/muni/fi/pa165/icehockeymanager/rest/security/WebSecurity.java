package cz.muni.fi.pa165.icehockeymanager.rest.security;

import cz.muni.fi.pa165.icehockeymanager.facades.UserAuthFacade;
import cz.muni.fi.pa165.icehockeymanager.security.Roles;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.List;

import static cz.muni.fi.pa165.icehockeymanager.rest.security.SecurityConstants.SIGN_UP_URL;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private final UserAuthFacade userAuthFacade;

    public WebSecurity(UserAuthFacade userAuthFacade) {
        this.userAuthFacade = userAuthFacade;
    }

    @Override
    @Order(1)
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().authorizeRequests()
                .antMatchers(HttpMethod.POST, SIGN_UP_URL).permitAll()
                .antMatchers(HttpMethod.GET, "/pa165/api/public/**").permitAll()
                .antMatchers("/pa165/api/manage/league/**").hasRole(Roles.LEAGUE_MANAGER.toString())
                .antMatchers("/pa165/api/manage/team/**").hasRole(Roles.TEAM_MANAGER.toString())
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthenticationFilter(userAuthFacade))
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), userAuthFacade))
                // this disables session creation on Spring Security
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.csrf().disable();
    }

//    @Bean
//    CorsConfigurationSource corsConfigurationSource() {
//        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//
//        CorsConfiguration corsConfiguration = new CorsConfiguration();
//        corsConfiguration.setAllowedMethods(
//                List.of("GET", "POST", "PUT", "DELETE")
//        );
//        corsConfiguration.addAllowedOriginPattern("*");
//        source.registerCorsConfiguration("/**", corsConfiguration);
//
//        return source;
//    }
}
