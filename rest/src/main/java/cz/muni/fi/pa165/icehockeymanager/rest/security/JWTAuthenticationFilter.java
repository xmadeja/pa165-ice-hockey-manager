package cz.muni.fi.pa165.icehockeymanager.rest.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.icehockeymanager.dto.UserAuthRequestDto;
import cz.muni.fi.pa165.icehockeymanager.facades.UserAuthFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import static cz.muni.fi.pa165.icehockeymanager.rest.security.SecurityConstants.*;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final UserAuthFacade userAuthFacade;
    Logger logger = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

    public JWTAuthenticationFilter(UserAuthFacade userAuthFacade) {
        this.userAuthFacade = userAuthFacade;

        setFilterProcessesUrl(AUTH_URL);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            UserAuthRequestDto creds = new ObjectMapper()
                    .readValue(req.getInputStream(), UserAuthRequestDto.class);
            logger.info(String.format("Log in attempt by %s", creds.getUsername()));
            var user = userAuthFacade.authenticateUser(
                    creds.getUsername(),
                    creds.getPassword()
            ).orElseThrow(() -> new BadCredentialsException(creds.getUsername()));
            return new UsernamePasswordAuthenticationToken(
                    creds.getUsername(),
                    creds.getPassword(),
                    List.of(
                            new SimpleGrantedAuthority(user.getRole().toString())
                    )
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException {
        logger.info(String.format("Generate JWT token for %s", auth.getPrincipal()));
        String token = JWT.create()
                .withSubject(((String) auth.getPrincipal()))
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.getBytes()));
        var role = auth.getAuthorities().stream().findFirst().orElseThrow().getAuthority();
        String body = auth.getPrincipal() + " " + role + " " + token;

        res.getWriter().write(body);
        res.getWriter().flush();
    }
}