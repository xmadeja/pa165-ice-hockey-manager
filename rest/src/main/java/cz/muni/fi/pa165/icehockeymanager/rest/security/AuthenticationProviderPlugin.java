package cz.muni.fi.pa165.icehockeymanager.rest.security;

import cz.muni.fi.pa165.icehockeymanager.facades.UserAuthFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class AuthenticationProviderPlugin implements AuthenticationProvider {

    private final UserAuthFacade userAuthFacade;

    @Autowired
    public AuthenticationProviderPlugin(UserAuthFacade userAuthFacade) {
        this.userAuthFacade = userAuthFacade;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        userAuthFacade.authenticateUser(username, password).orElseThrow(() -> new BadCredentialsException(username));
        return new UsernamePasswordAuthenticationToken(
                username, password, new ArrayList<>());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
