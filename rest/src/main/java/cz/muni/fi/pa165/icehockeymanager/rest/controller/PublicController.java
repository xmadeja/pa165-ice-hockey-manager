package cz.muni.fi.pa165.icehockeymanager.rest.controller;

import cz.muni.fi.pa165.icehockeymanager.dto.GameDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.TeamDto;
import cz.muni.fi.pa165.icehockeymanager.facades.TeamManagerFacade;
import cz.muni.fi.pa165.icehockeymanager.facades.UserFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/pa165/api/public")
public class PublicController {

    private final UserFacade userFacade;
    private final TeamManagerFacade teamManagerFacade;

    public PublicController(UserFacade userFacade, TeamManagerFacade teamManagerFacade) {
        this.userFacade = userFacade;
        this.teamManagerFacade = teamManagerFacade;
    }

    @GetMapping("/player/all")
    public final Collection<PlayerDto> getAllPlayers() {
        return userFacade.getAllPlayers();
    }

    @GetMapping("/player/free")
    public final Collection<PlayerDto> getFreePlayers() {
        return teamManagerFacade.getFreePlayers();
    }

    @GetMapping("/player/{id}")
    public final PlayerDto findPlayer(@PathVariable Long id) {
        return userFacade.findPlayer(id).orElse(null);
    }

    @GetMapping("/player/team/{id}")
    public final Collection<PlayerDto> getPlayersInTeam(@PathVariable Long id) {
        return userFacade.getPlayersInTeam(id);
    }

    @GetMapping("/team/all")
    public final Collection<TeamDto> getTeamsInLeague() {
        return userFacade.getTeamsInLeague();
    }

    @GetMapping("/game/team/{id}")
    public final Collection<GameDto> getGamesForTeam(@PathVariable Long id) {
        return userFacade.getGamesForTeam(id);
    }

    @GetMapping("/game/all")
    public final Collection<GameDto> getGamesForLeague() {
        return userFacade.getGamesForLeague();
    }
}
