package cz.muni.fi.pa165.icehockeymanager.rest.demo;

import cz.muni.fi.pa165.icehockeymanager.dto.*;
import cz.muni.fi.pa165.icehockeymanager.facades.LeagueManagerFacade;
import cz.muni.fi.pa165.icehockeymanager.facades.TeamManagerFacade;
import cz.muni.fi.pa165.icehockeymanager.facades.UserAuthFacade;
import cz.muni.fi.pa165.icehockeymanager.facades.UserFacade;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class DataLoader implements ApplicationRunner {

    private final UserAuthFacade userAuth;
    private final LeagueManagerFacade leagueManager;
    private final TeamManagerFacade teamManagerFacade;
    private final UserFacade userFacade;

    public DataLoader(UserAuthFacade userAuth, LeagueManagerFacade leagueManager, TeamManagerFacade teamManagerFacade, UserFacade userFacade) {
        this.userAuth = userAuth;
        this.leagueManager = leagueManager;
        this.teamManagerFacade = teamManagerFacade;
        this.userFacade = userFacade;
    }

    private static TeamCreateDto createTeam(String name) {
        var team = new TeamCreateDto();
        team.setName(name);
        return team;
    }

    private static PlayerCreateDto createPlayer(String name, long teamId) {
        var player = new PlayerCreateDto();
        player.setName(name);
        player.setTeamId(teamId);
        return player;
    }

    private static PlayerTransferDto transferPlayer(long playerId, long teamId) {
        var transfer = new PlayerTransferDto();
        transfer.setPlayerId(playerId);
        transfer.setTeamId(teamId);
        return transfer;
    }

    private static GameCreateDto createGame(long homeTeamId, long awayTeamId) {
        var game = new GameCreateDto();
        game.setHomeTeamId(homeTeamId);
        game.setAwayTeamId(awayTeamId);
        game.setAwayTeamScore(ThreadLocalRandom.current().nextInt(0, 10));
        game.setHomeTeamScore(ThreadLocalRandom.current().nextInt(0, 10));
        if (game.getHomeTeamScore() > game.getAwayTeamScore()) {
            game.setWinnerId(game.getHomeTeamId());
        }
        if (game.getHomeTeamScore() < game.getAwayTeamScore()) {
            game.setWinnerId(game.getAwayTeamId());
        }
        game.setGameDateTime(ZonedDateTime.now());
        return game;
    }

    @Override
    public void run(ApplicationArguments args) {
        leagueManager.createNewTeam(createTeam("Toronto Maple Leafs"));
        leagueManager.createNewTeam(createTeam("HC Slovan Bratislava"));
        List<TeamDto> teams = new ArrayList<>(userFacade.getTeamsInLeague());

        var team = teams.get(0).getId();
        var team2 = teams.get(1).getId();

        teamManagerFacade.recruitNewPlayer(createPlayer("Marian Hossa", team));
        teamManagerFacade.recruitNewPlayer(createPlayer("Marcel Hossa", team2));
        teamManagerFacade.recruitNewPlayer(createPlayer("Zdeno Chára", team));
        teamManagerFacade.recruitNewPlayer(createPlayer("Miroslav Šatan", team2));

        leagueManager.createGame(createGame(team, team2));
        leagueManager.createGame(createGame(team2, team));

        userAuth.createTeamManager("team_manager", "1234", team.intValue());
        userAuth.createTeamManager("team_manager_slovan", "0000", team2.intValue());
        userAuth.createLeagueManager("league_manager", "9876");
    }
}
