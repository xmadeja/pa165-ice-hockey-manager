package cz.muni.fi.pa165.icehockeymanager.rest.controller;

import cz.muni.fi.pa165.icehockeymanager.dto.GameCreateDto;
import cz.muni.fi.pa165.icehockeymanager.dto.GameUpdateScoreDto;
import cz.muni.fi.pa165.icehockeymanager.dto.GameWinnerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.TeamCreateDto;
import cz.muni.fi.pa165.icehockeymanager.facades.LeagueManagerFacade;
import cz.muni.fi.pa165.icehockeymanager.rest.security.JWTAuthenticationFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/pa165/api/manage/league")
public class LeagueManagerController {

    private final LeagueManagerFacade leagueManagerFacade;
    Logger logger = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

    public LeagueManagerController(LeagueManagerFacade leagueManagerFacade) {
        this.leagueManagerFacade = leagueManagerFacade;
    }

    @PostMapping("/team/create")
    public final void createTeam(@Valid @RequestBody TeamCreateDto team) {
        logger.info("Create Team request recieved");
        leagueManagerFacade.createNewTeam(team);
    }

    @PostMapping("/game/create")
    public final void createGame(@Valid @RequestBody GameCreateDto game) {
        leagueManagerFacade.createGame(game);
    }

    @PutMapping("/game/update")
    public final void updateGameScore(@Valid @RequestBody GameUpdateScoreDto game) {
        leagueManagerFacade.updateGameScore(game);
    }

    @PutMapping("/game/setWinner")
    public final void setGameWinner(@Valid @RequestBody GameWinnerDto game) {
        leagueManagerFacade.setGameWinner(game);
    }

    @DeleteMapping("/game/delete/{id}")
    public final void dropGame(@PathVariable Long id) {
        leagueManagerFacade.dropGame(id);
    }
}
