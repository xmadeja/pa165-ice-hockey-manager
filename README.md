# PV165 Ice Hockey Manager

Team:
* [Dovjak, Ján](https://www.muni.cz/en/people/469235-jan-dovjak)
* [Madeja, Tomáš](https://www.muni.cz/en/people/456443-tomas-madeja)
* [Moravec, Erik](https://www.muni.cz/en/people/468981-erik-moravec)

## Description

Several human players (at least two) can manage their hockey teams out of a list of real ice hockey teams of several championships across Europe. Human players can pick their team and add / remove ice hockey players from a list of available free agents. There is a schedule of games and results will be generated taking into account the players characteristics (no need to have some advanced algorithm simulating games: just a simple randomization will do it!). Admin can put new hockey players in the main list of free agents and change their attributes before they are selected by other human players. If you want, you can implement a budget system for each team, so that players can be bought and sold based on the financial availability of teams.
* The project description has been a subject to many changes after consultations with the customer/teacher

## Milestones

1st Milestone
* project done: Sun 04.04.2021 23:59
* evaluation submitted: Sun 11.04.2021 23:59

2nd Milestone
* project done: Sun 25.04.2021 23:59
* evaluation submitted: Sun 02.05.2021 23:59

3rd Milestone
* project done: Sun 01.06.2021 23:59
* evaluation submitted: Fri 04.06.2021 23:59  (note the deadline on Friday this time)

## Setup

1. Install npm and Node.js - https://www.npmjs.com/get-npm (check the option to install all required dependencies)
2. Go to the web folder and run "npm update" in your shell (this might take a while, it is expected)
3. Run "npm install -g @angular/cli" in your shell

## Run

1. In the root folder run "mvn clean install" in your shell (can be done in intelliJ idea, be sure to run it in the root)
2. In directory /rest run "mvn spring-boot:run", this will start REST API web application (can be done in idea by building and running the RestApplication)
3. In directory /web run "ng serve --open", this will open the web application in your browser (if the REST API was started using shell, you will probably need a separate shell for this).

The application is listening on http://localhost:4200/

## Security

There are three roles in the application. By default you are the User and can only use/access the public API, such as list all teams or players.

The second role is the Team manager. Login using following credentials, that will allow you to manage your hockey team:
* username: team_manager
* password: 1234

The third role is the League manager. Login using following credentials, that will allow you to schedule and edit hockey matches and to add new teams into the league:
* username: league_manager
* password: 9876

## Rest

You can use public API after starting REST API application. We recommend to use Postman (https://www.postman.com/),
or you can enter the links to the browser directly while the Rest API is running (how to run it is explained above).
However, everything in the web application was done using only calls to the Rest API, so it is fine if you just test the UI.

Public API calls (all of them are GET):

Get all players
http://localhost:8080/pa165/api/public/player/all

Get all free players
http://localhost:8080/pa165/api/public/player/free

Get one player by id
http://localhost:8080/pa165/api/public/player/%7Bid%7D

Get all player in team with id
http://localhost:8080/pa165/api/public/player/team/%7Bid%7D

Get all teams 
http://localhost:8080/pa165/api/public/player/team/all

Get all games
http://localhost:8080/pa165/api/public/game/all
