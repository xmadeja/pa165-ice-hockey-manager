package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.dao.PlayerDao;
import cz.muni.fi.pa165.icehockeymanager.exceptions.PlayerNotInTeamException;
import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService {
    private final PlayerDao playerDao;

    @Autowired
    public PlayerServiceImpl(PlayerDao playerDao) {
        this.playerDao = playerDao;
    }

    @Override
    public Optional<Player> findPlayer(long id) {
        return playerDao.read(id);
    }

    @Override
    public void createPlayer(Player player, Team team) {
        player.setTeam(team);
        playerDao.create(player);
    }

    @Override
    public Collection<Player> getFreePlayers() {
        return playerDao.getAllFreePlayers();
    }

    @Override
    public Collection<Player> getAllPlayers() {
        return playerDao.readAll();
    }

    @Override
    public void addTeamToPlayer(Player player, Team team) {
        player.setTeam(team);
        playerDao.update(player);
    }

    @Override
    public void removeTeamFromPlayer(Player player, Team team) {
        if(player.getTeam().isEmpty() || !player.getTeam().get().equals(team)){
            throw new PlayerNotInTeamException("Player not in this team");
        }
        player.setTeam(null);
        playerDao.update(player);
    }
}
