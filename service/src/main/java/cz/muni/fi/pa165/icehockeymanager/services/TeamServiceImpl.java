package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.dao.TeamDao;
import cz.muni.fi.pa165.icehockeymanager.exceptions.PlayerNotInTeamException;
import cz.muni.fi.pa165.icehockeymanager.model.Account;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import cz.muni.fi.pa165.icehockeymanager.security.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class TeamServiceImpl implements TeamService {

    private final TeamDao teamDao;

    @Autowired
    public TeamServiceImpl(TeamDao teamDao) {
        this.teamDao = teamDao;
    }

    @Override
    public Collection<Team> getAllTeams() {
        return teamDao.readAll();
    }

    @Override
    public Optional<Team> findTeam(long id) {
        return teamDao.read(id);
    }

    @Override
    public Optional<Team> findTeamByManager(Account manager) {
        Objects.requireNonNull(manager);
        return teamDao.findByManager(manager);
    }

    @Override
    public void addPlayerToTeam(Team team, Player player) {
        if (player == null){
            throw new NullPointerException("Player is null");
        }
        team.addPlayer(player);
        teamDao.update(team);
    }

    @Override
    public void removePlayerFromTeam(Team team, Player player) {
        if(!team.containsPlayer(player)){
            throw new PlayerNotInTeamException("Player not in this team");
        }
        team.removePlayer(player);
        teamDao.update(team);
    }

    @Override
    public void addHomeGame(Team homeTeam, Game game) {
        if (game == null) {
            throw new NullPointerException("The game is null");
        }
        homeTeam.addHomeGame(game);
        teamDao.update(homeTeam);
    }

    @Override
    public void addAwayGame(Team awayTeam, Game game) {
        if (game == null) {
            throw new NullPointerException("The game is null");
        }
        awayTeam.addAwayGame(game);
        teamDao.update(awayTeam);
    }

    @Override
    public void removeHomeGame(Team homeTeam, Game game) {
        if (game == null) {
            throw new NullPointerException("The game is null");
        }
        homeTeam.removeHomeGame(game);
        teamDao.update(homeTeam);
    }

    @Override
    public void removeAwayGame(Team awayTeam, Game game) {
        if (game == null) {
            throw new NullPointerException("The game is null");
        }
        awayTeam.removeAwayGame(game);
        teamDao.update(awayTeam);
    }

    @Override
    public void createTeam(Team team) {
        teamDao.create(team);
    }

    @Override
    public void updateTeamManager(Team team, Account manager) {
        if (!manager.getRole().equals(Roles.TEAM_MANAGER.toString())) {
            throw new IllegalArgumentException("Manager is not a team manager");
        }
        team.setManagerId(manager.getId());
        teamDao.update(team);
    }
}
