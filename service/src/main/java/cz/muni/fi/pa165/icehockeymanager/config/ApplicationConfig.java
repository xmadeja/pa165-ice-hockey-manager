package cz.muni.fi.pa165.icehockeymanager.config;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories
@Import(PersistanceApplicationConfig.class)
@ComponentScan(basePackages = "cz.muni.fi.pa165.icehockeymanager")
public class ApplicationConfig {

    @Bean
    public Mapper dozer() {
        var mapperBuilder = DozerBeanMapperBuilder.create();
        mapperBuilder.withMappingBuilder(new DozerConfig());
        return mapperBuilder.build();
    }

    @Bean
    public Argon2PasswordEncoder argon2Encoder() {
        /*
        Recommended parameters:
            - hash and salt - each at least 16
            - parallellism - twice the core count
            - memory - at least 4 GB
            - iterations - adjustable
        Note: adjust memory and iterations to hit response time: 0.5-1 sec
         */
        int cores = Runtime.getRuntime().availableProcessors();
        return new Argon2PasswordEncoder(32, 32, cores*2, 976563, 1);
    }
}
