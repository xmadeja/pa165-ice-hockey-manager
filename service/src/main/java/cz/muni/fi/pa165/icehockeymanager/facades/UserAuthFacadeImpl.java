package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.dto.TeamDto;
import cz.muni.fi.pa165.icehockeymanager.dto.UserDto;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownTeamException;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownUserException;
import cz.muni.fi.pa165.icehockeymanager.model.Account;
import cz.muni.fi.pa165.icehockeymanager.security.Roles;
import cz.muni.fi.pa165.icehockeymanager.services.BeanMappingService;
import cz.muni.fi.pa165.icehockeymanager.services.TeamService;
import cz.muni.fi.pa165.icehockeymanager.services.UserAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class UserAuthFacadeImpl implements UserAuthFacade {

    private final UserAuthService authService;
    private final TeamService teamService;
    private final BeanMappingService beanMapper;

    @Autowired
    public UserAuthFacadeImpl(
            UserAuthService authService,
            TeamService teamService, BeanMappingService beanMapper) {
        this.authService = authService;
        this.teamService = teamService;
        this.beanMapper = beanMapper;
    }

    @Override
    public Optional<UserDto> authenticateUser(String username, String password) {
        return authService
                .authenticateUser(username, password)
                .flatMap(user -> Optional.of(beanMapper.mapTo(user, UserDto.class)));
    }

    @Override
    public UserDto createLeagueManager(String username, String password) {
        var user = authService.createLeagueManager(username, password);
        return beanMapper.mapTo(user, UserDto.class);
    }

    @Override
    public UserDto createTeamManager(String username, String password, int teamId) {
        var team = teamService.findTeam(teamId)
                .orElseThrow(() -> new UnknownTeamException(Integer.toString(teamId)));
        Account user = authService.createTeamManager(username, password);
        teamService.updateTeamManager(team, user);
        return beanMapper.mapTo(user, UserDto.class);
    }

    @Override
    public Optional<TeamDto> fetchTeamForManager(String username) {
        var user = authService.findUser(username).orElseThrow(UnknownUserException::new);
        if (!user.getRole().equals(Roles.TEAM_MANAGER.toString())) {
            return Optional.empty();
        }
        return teamService
                .findTeamByManager(user)
                .flatMap(team -> Optional.of(beanMapper.mapTo(team, TeamDto.class)));
    }

    @Override
    public Optional<UserDto> fetchUser(String username) {
        return authService
                .findUser(username)
                .flatMap(user -> Optional.of(beanMapper.mapTo(user, UserDto.class)));
    }
}
