package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.exceptions.UsernameTakenException;
import cz.muni.fi.pa165.icehockeymanager.model.Account;

import java.util.Optional;

public interface UserAuthService {

    /**
     * Authenticate user
     * @param username username
     * @param password plaintext password
     * @return Empty optional if user was not found or password did not match
     */
    Optional<Account> authenticateUser(String username, String password);

    /**
     * Create new League Manager account
     * @param username username
     * @param password plaintext password
     * @return User if exists
     *
     * @throws UsernameTakenException if username is already taken by another user
     */
    Account createLeagueManager(String username, String password);

    /**
     * Create new Team Manager account
     * @param username username
     * @param password plaintext password
     * @return User if exists
     *
     * @throws UsernameTakenException if username is already taken by another user
     */
    Account createTeamManager(String username, String password);

    /**
     * Authenticate user
     * @param username username
     * @return Empty optional if user was not found
     */
    Optional<Account> findUser(String username);
}
