package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.dao.GameDao;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownTeamException;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

@Service
@Transactional
public class GameServiceImpl implements GameService {

    public final GameDao gameDao;

    @Autowired
    public GameServiceImpl(GameDao gameDao) {
        this.gameDao = gameDao;
    }

    @Override
    public Collection<Game> getAllGames() {
        return gameDao.readAll();
    }

    @Override
    public void createGame(Game game) {
        gameDao.create(game);
    }

    @Override
    public Optional<Game> findGame(long gameId) {
        return gameDao.read(gameId);
    }

    @Override
    public void deleteGame(Game game) {
        gameDao.delete(game);
    }

    @Override
    public void updateGameScore(Game game, int homeTeamScore, int awayTeamScore) {
        game.setHomeTeamScore(homeTeamScore);
        game.setAwayTeamScore(awayTeamScore);
        if (homeTeamScore > awayTeamScore) {
            game.setWinner(game.getHomeTeam());
        } else if (awayTeamScore > homeTeamScore) {
            game.setWinner(game.getAwayTeam());
        } else {
            game.setWinner(null);
        }
        gameDao.update(game);
    }

    @Override
    public void setGameWinner(Game game, Team team) {
        if (team != null &&!team.equals(game.getHomeTeam()) && !team.equals(game.getAwayTeam())) {
            throw new UnknownTeamException("The winner is not part of this game");
        }
        game.setWinner(team);
        gameDao.update(game);
    }
}
