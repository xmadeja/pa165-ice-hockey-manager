package cz.muni.fi.pa165.icehockeymanager.services;

import com.github.dozermapper.core.Mapper;
import cz.muni.fi.pa165.icehockeymanager.dto.GameDto;
import cz.muni.fi.pa165.icehockeymanager.dto.TeamDto;
import cz.muni.fi.pa165.icehockeymanager.dto.UserDto;
import cz.muni.fi.pa165.icehockeymanager.model.Account;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.security.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BeanMappingServiceImpl implements BeanMappingService {

    private final Mapper dozer;

    @Autowired
    public BeanMappingServiceImpl(Mapper dozer) {
        this.dozer = dozer;
    }

    public <T> List<T> mapTo(Collection<?> objects, Class<T> mapToClass) {
        List<T> mappedCollection = new ArrayList<>();
        for (Object object : objects) {
            mappedCollection.add(mapTo(object, mapToClass));
        }
        return mappedCollection;
    }

    public <T> T mapTo(Object u, Class<T> mapToClass) {
        if (u == null)
            return null;
        return dozer.map(u, mapToClass);
    }

    public GameDto mapTo(Game u, Class<GameDto> mapToClass) {
        if (u == null)
            return null;
        GameDto mappedObject = dozer.map(u, mapToClass);
        TeamDto winner = mapTo(u.getWinner().orElse(null), TeamDto.class);
        mappedObject.setWinner(winner);
        return mappedObject;
    }

    public UserDto mapTo(Account u, Class<UserDto> mapToClass) {
        if (u == null)
            return null;
        UserDto mappedObject = dozer.map(u, mapToClass);
        Arrays.stream(Roles.values())
                .filter(role -> roleToString(role).equals(u.getRole()))
                .forEach(mappedObject::setRole);
        return mappedObject;
    }

    @Override
    public List<GameDto> mapCollection(Collection<Game> objects, Class<GameDto> mapToClass) {
        return objects.stream().map((game) -> mapTo(game, GameDto.class)).collect(Collectors.toList());
    }

    public Mapper getMapper() {
        return dozer;
    }

    private static String roleToString(Roles role) {
        return role.name().toLowerCase();
    }
}