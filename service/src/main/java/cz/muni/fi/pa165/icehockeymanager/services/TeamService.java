package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.model.Account;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;

import java.util.Collection;
import java.util.Optional;

/**
 * Service exposing storage, update, and retriaval of Ice Hockey Teams.
 */
public interface TeamService {

    /**
     * Retrieve all stored teams.
     *
     * @return teams
     */
    Collection<Team> getAllTeams();

    /**
     * Retrieve team by unique identifier.
     *
     * @param id Unique identifier of a team
     * @return Team, or null iff no team for such ID was found
     */
    Optional<Team> findTeam(long id);

    /**
     * Retrieve team by manager.
     *
     * @param manager Unique identifier of a team
     * @return Team, or null iff no team for such ID was found
     */
    Optional<Team> findTeamByManager(Account manager);

    /**
     * Adds the player to a team.
     *
     * @param team Team of the player
     * @param player The player to be updated
     */
    void addPlayerToTeam(Team team, Player player);

    /**
     * Removes the player from a team
     *
     * @param team Team of the player
     * @param player The player to be updated
     */
    void removePlayerFromTeam(Team team, Player player);

    /**
     * Add home game to the team
     *
     * @param homeTeam The team to be updated
     * @param game Game to be added as home game to the team.
     */
    void addHomeGame(Team homeTeam, Game game);

    /**
     * Add away game to the team
     *
     * @param awayTeam The team to be updated
     * @param game Game to be added as away game to the team.
     */
    void addAwayGame(Team awayTeam, Game game);

    /**
     * Removes home game from the team
     *
     * @param homeTeam The team to be updated
     * @param game Game to be removed from home games
     */
    void removeHomeGame(Team homeTeam, Game game);

    /**
     * Removes away game from the team
     *
     * @param awayTeam The team to be updated
     * @param game Game to be removed from away games
     */
    void removeAwayGame(Team awayTeam, Game game);

    /**
     * Creates the team entity
     *
     * @param team the team to be created
     */
    void createTeam(Team team);

    /**
     * Set a new team manager
     * @param team team
     * @param manager team manager user
     */
    void updateTeamManager(Team team, Account manager);
}
