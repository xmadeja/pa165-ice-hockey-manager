package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.dto.PlayerCreateDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerTransferDto;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownPlayerException;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownTeamException;
import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import cz.muni.fi.pa165.icehockeymanager.services.BeanMappingService;
import cz.muni.fi.pa165.icehockeymanager.services.PlayerService;
import cz.muni.fi.pa165.icehockeymanager.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class TeamManagerFacadeImpl implements TeamManagerFacade{

    private final PlayerService playerService;

    private final TeamService teamService;

    private final BeanMappingService beanMappingService;

    @Autowired
    public TeamManagerFacadeImpl(PlayerService playerService, TeamService teamService, BeanMappingService beanMappingService) {
        this.playerService = playerService;
        this.teamService = teamService;
        this.beanMappingService = beanMappingService;
    }

    @Override
    public void recruitNewPlayer(PlayerCreateDto playerCreateDTO) {
        Team team = teamService.findTeam(playerCreateDTO.getTeamId()).orElseThrow(
                () -> new UnknownTeamException("Team not found")
        );
        Player newPlayer = beanMappingService.mapTo(playerCreateDTO, Player.class);
        playerService.createPlayer(newPlayer, team);
        teamService.addPlayerToTeam(team, newPlayer);
    }

    @Override
    public void recruitPlayer(PlayerTransferDto playerDto){
        Team team = teamService.findTeam(playerDto.getTeamId()).orElseThrow(
                () -> new UnknownTeamException("Team not found")
        );
        Player player = playerService.findPlayer(playerDto.getPlayerId()).orElseThrow(
                () -> new UnknownPlayerException("Player not found")
        );
        teamService.addPlayerToTeam(team, player);
        playerService.addTeamToPlayer(player, team);
    }

    @Override
    public Collection<PlayerDto> getFreePlayers() {
        var players = playerService.getFreePlayers();
        return beanMappingService.mapTo(players, PlayerDto.class);
    }

    @Override
    public void firePlayer(long playerId) {
        Player player = playerService.findPlayer(playerId).orElseThrow(
                () -> new UnknownPlayerException("Player not found")
        );
        Team playerTeam = player.getTeam().orElseThrow(
                () -> new UnknownTeamException("Player not in the team")
        );
        Team team = teamService.findTeam(playerTeam.getId()).orElseThrow(
                () -> new UnknownTeamException("Team not found")
        );
        teamService.removePlayerFromTeam(team, player);
        playerService.removeTeamFromPlayer(player, team);
    }
}
