package cz.muni.fi.pa165.icehockeymanager.config;

import com.github.dozermapper.core.loader.api.BeanMappingBuilder;
import cz.muni.fi.pa165.icehockeymanager.dto.*;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;

public class DozerConfig extends BeanMappingBuilder {
    @Override
    protected void configure() {
        mapping(Player.class, PlayerDto.class);
        mapping(Player.class, PlayerCreateDto.class);
        mapping(Team.class, TeamDto.class);
        mapping(Team.class, TeamCreateDto.class);
        mapping(Game.class, GameDto.class);
        mapping(Game.class, GameCreateDto.class);
    }
}
