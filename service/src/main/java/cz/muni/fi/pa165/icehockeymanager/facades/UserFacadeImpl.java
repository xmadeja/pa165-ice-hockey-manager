package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.dto.GameDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.TeamDto;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownTeamException;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import cz.muni.fi.pa165.icehockeymanager.services.BeanMappingService;
import cz.muni.fi.pa165.icehockeymanager.services.GameService;
import cz.muni.fi.pa165.icehockeymanager.services.PlayerService;
import cz.muni.fi.pa165.icehockeymanager.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserFacadeImpl implements UserFacade {

    private final PlayerService playerService;

    private final TeamService teamService;

    private final GameService gameService;

    private final BeanMappingService beanMappingService;

    @Autowired
    public UserFacadeImpl(PlayerService playerService, TeamService teamService, GameService gameService, BeanMappingService beanMappingService) {
        this.playerService = playerService;
        this.teamService = teamService;
        this.gameService = gameService;
        this.beanMappingService = beanMappingService;
    }

    @Override
    public Collection<GameDto> getGamesForTeam(long id) {
        Team team = teamService.findTeam(id).orElseThrow(
                () -> new UnknownTeamException("Team not found")
        );
        List<Game> games = new ArrayList<>(team.getHomeGames());
        games.addAll(team.getAwayGames());
        return beanMappingService.mapCollection(games, GameDto.class);
    }

    @Override
    public Collection<GameDto> getGamesForLeague() {
        var games = gameService.getAllGames();
        return beanMappingService.mapCollection(games, GameDto.class);
    }

    @Override
    public Optional<PlayerDto> findPlayer(long id) {
        var player = playerService.findPlayer(id);
        if (player.isEmpty())
            return Optional.empty();
        return Optional.ofNullable(beanMappingService.mapTo(player.get(), PlayerDto.class));
    }

    @Override
    public Collection<PlayerDto> getPlayersInTeam(long id) {
        Team team = teamService.findTeam(id).orElseThrow(
                () -> new UnknownTeamException("Team not found")
        );
        return beanMappingService.mapTo(team.getPlayers(), PlayerDto.class);
    }

    @Override
    public Collection<PlayerDto> getAllPlayers() {
        var players = playerService.getAllPlayers();
        return beanMappingService.mapTo(players, PlayerDto.class);
    }

    @Override
    public Collection<TeamDto> getTeamsInLeague() {
        var teams = teamService.getAllTeams();
        return beanMappingService.mapTo(teams, TeamDto.class);
    }
}
