package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.dto.GameCreateDto;
import cz.muni.fi.pa165.icehockeymanager.dto.GameUpdateScoreDto;
import cz.muni.fi.pa165.icehockeymanager.dto.GameWinnerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.TeamCreateDto;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownGameException;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownTeamException;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import cz.muni.fi.pa165.icehockeymanager.services.BeanMappingService;
import cz.muni.fi.pa165.icehockeymanager.services.GameService;
import cz.muni.fi.pa165.icehockeymanager.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;

@Service
@Transactional
public class LeagueManagerFacadeImpl implements LeagueManagerFacade {

    private final TeamService teamService;
    private final BeanMappingService beanMappingService;
    private final GameService gameService;

    @Autowired
    public LeagueManagerFacadeImpl(TeamService teamService, BeanMappingService beanMappingService, GameService gameService) {
        this.teamService = teamService;
        this.beanMappingService = beanMappingService;
        this.gameService = gameService;
    }

    @Override
    public void createGame(GameCreateDto gameCreateDTO) {
        if (gameCreateDTO.getGameDateTime() == null) {
            gameCreateDTO.setGameDateTime(ZonedDateTime.parse("2000-01-01T00:04:20+00:00"));
        }
        Game game = beanMappingService.mapTo(gameCreateDTO, Game.class);
        Team awayTeam = teamService.findTeam(gameCreateDTO.getAwayTeamId()).orElse(null);
        Team homeTeam = teamService.findTeam(gameCreateDTO.getHomeTeamId()).orElse(null);
        game.setAwayTeam(awayTeam);
        game.setHomeTeam(homeTeam);
        gameService.createGame(game);
        teamService.addHomeGame(game.getHomeTeam(), game);
        teamService.addAwayGame(game.getAwayTeam(), game);
    }

    @Override
    public void dropGame(long gameId) {
        Game game = gameService.findGame(gameId).orElseThrow(
                () -> new UnknownGameException("Game not found")
        );
        gameService.deleteGame(game);
        teamService.removeHomeGame(game.getHomeTeam(), game);
        teamService.removeAwayGame(game.getAwayTeam(), game);
    }

    @Override
    public void updateGameScore(GameUpdateScoreDto gameDTO) {
        Game game = gameService.findGame(gameDTO.getId()).orElseThrow(
                () -> new UnknownGameException("Game not found")
        );
        gameService.updateGameScore(game, gameDTO.getHomeTeamScore(), gameDTO.getAwayTeamScore());
    }

    @Override
    public void setGameWinner(GameWinnerDto gameWinner) {
        Game game = gameService.findGame(gameWinner.getId()).orElseThrow(
                () -> new UnknownGameException("Game not found")
        );
        Team team = teamService.findTeam(gameWinner.getTeamId()).orElseThrow(
                () -> new UnknownTeamException("Team not found")
        );
        gameService.setGameWinner(game, team);
    }

    @Override
    public void createNewTeam(TeamCreateDto teamCreateDTO) {
        Team team = beanMappingService.mapTo(teamCreateDTO, Team.class);
        teamService.createTeam(team);
    }
}
