package cz.muni.fi.pa165.icehockeymanager.services;


import com.github.dozermapper.core.Mapper;
import cz.muni.fi.pa165.icehockeymanager.dto.GameDto;
import cz.muni.fi.pa165.icehockeymanager.dto.UserDto;
import cz.muni.fi.pa165.icehockeymanager.model.Account;
import cz.muni.fi.pa165.icehockeymanager.model.Game;

import java.util.Collection;
import java.util.List;

/**
 * Service for mapping Beans to DTOs.
 */
public interface BeanMappingService {

    /**
     * Map collection to DTO
     *
     * @param objects    Collection of source beans
     * @param mapToClass destination DTO class
     * @param <T>        DTO class
     * @return List of DTO objects
     */
    <T> List<T> mapTo(Collection<?> objects, Class<T> mapToClass);

    /**
     * Map object to DTO
     *
     * @param u          Source bean
     * @param mapToClass Destination DTO class
     * @param <T>        DTO class
     * @return DTO object
     */
    <T> T mapTo(Object u, Class<T> mapToClass);

    /**
     * Retrieve mapper instance.
     *
     * @return Mapper instance
     */
    Mapper getMapper();

    GameDto mapTo(Game u, Class<GameDto> mapToClass);

    UserDto mapTo(Account u, Class<UserDto> mapToClass);

    List<GameDto> mapCollection(Collection<Game> objects, Class<GameDto> mapToClass);
}