package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Team;

import java.util.Collection;
import java.util.Optional;

/**
 * Service exposing storage, update, and retriaval of Ice Hockey Games.
 */
public interface GameService {

    /**
     * Retrieve all stored games.
     *
     * @return games
     */
    Collection<Game> getAllGames();

    /**
     * Create a new entity game in DB.
     * @param game the game to be created.
     */
    void createGame(Game game);

    /**
     * Finds the game by given id.
     * @param gameId ID of the game to be found.
     * @return return Optional of the game to be found.
     */
    Optional<Game> findGame(long gameId);

    /**
     * Removes a game entity from DB.
     * @param game the entity to be removed
     */
    void deleteGame(Game game);

    /**
     * Updates the score of a game and the winner if a winner is
     * determined by the new score.
     * @param game the entity to be updated.
     * @param homeTeamScore new score of the home team.
     * @param awayTeamScore new score of the away team.
     */
    void updateGameScore(Game game, int homeTeamScore, int awayTeamScore);

    /**
     * Sets the winner of a game.
     * @param game the entity to be updated.
     * @param team the new winner of the game.
     */
    void setGameWinner(Game game, Team team);
}
