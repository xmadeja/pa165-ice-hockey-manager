package cz.muni.fi.pa165.icehockeymanager.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class Argon2ServiceImpl implements Argon2Service {
    private final Argon2PasswordEncoder encoder;

    @Autowired
    public Argon2ServiceImpl(Argon2PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    public String encodePassword(CharSequence plaintext) {
        return encoder.encode(plaintext);
    }

    public boolean compare(CharSequence plaintext, String encoded) {
        return encoder.matches(plaintext, encoded);
    }
}
