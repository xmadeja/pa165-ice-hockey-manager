package cz.muni.fi.pa165.icehockeymanager.services;

public interface Argon2Service {

    /**
     * Encodes password as Argon2 hash.
     * The encoded password includes the has.
     * @param plaintext plaintext password
     * @return encoded password
     */
    String encodePassword(CharSequence plaintext);

    /**
     * Compares plaintext password with encoded counterpart.
     * @param plaintext plaintext password
     * @param encoded argon2 encoded password
     * @return true if the passwords match, false otherwise
     */
    boolean compare(CharSequence plaintext, String encoded);
}
