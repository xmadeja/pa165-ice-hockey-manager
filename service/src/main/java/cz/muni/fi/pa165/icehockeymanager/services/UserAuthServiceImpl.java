package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.dao.UserAuthDao;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UsernameTakenException;
import cz.muni.fi.pa165.icehockeymanager.model.Account;
import cz.muni.fi.pa165.icehockeymanager.security.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserAuthServiceImpl implements UserAuthService{

    public final Argon2Service argonEncoder;
    public final UserAuthDao userAuthDao;


    @Autowired
    public UserAuthServiceImpl(
            Argon2Service argonEncoder,
            UserAuthDao userAuthDao
    ) {
        this.argonEncoder = argonEncoder;
        this.userAuthDao = userAuthDao;
    }

    public Optional<Account> authenticateUser(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        requireNonEmpty(username, "username");
        requireNonEmpty(password, "password");

        Optional<Account> userOpt = userAuthDao.findByUsername(username);

        return userOpt.flatMap(user -> {
            if (argonEncoder.compare(password, user.getPassword())) {
                return Optional.of(user);
            }
            return Optional.empty();
        });
    }

    public Account createLeagueManager(String username, String password) {
        return createUser(username, password, Roles.LEAGUE_MANAGER);
    }

    public Account createTeamManager(String username, String password) {
        return createUser(username, password, Roles.TEAM_MANAGER);
    }

    @Override
    public Optional<Account> findUser(String username) {
        return userAuthDao.findByUsername(username);
    }

    private void requireNonEmpty(String argument, String paramName) {
        if (argument.isEmpty()) {
            throw new IllegalArgumentException(paramName);
        }
    }

    private Account createUser(String username, String password, Roles role) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);
        Objects.requireNonNull(role);
        requireNonEmpty(username, "username");
        requireNonEmpty(password, "password");

        // first get password, to introduce time cost of username lookup
        String encodedPassword = argonEncoder.encodePassword(password);

        var user = new Account();
        user.setUsername(username);
        user.setPassword(encodedPassword);
        user.setRole(getRoleString(role));
        try {
            userAuthDao.create(user);
        } catch (EntityExistsException e) {
            throw new UsernameTakenException(username);
        }
        return user;
    }

    private String getRoleString(Roles role) {
        return role.toString();
    }
}
