package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;

import java.util.Collection;
import java.util.Optional;

/**
 * Service exposing storage, update, and retriaval of Ice Hockey Players.
 */
public interface PlayerService {

    /**
     * Retrieve player by an identifier.
     *
     * @param id Unique identifier of a player
     * @return Player, or empty iff no player for such ID was found
     */
    Optional<Player> findPlayer(long id);

    /**
     * Creates the player entity in the DB.
     * @param player the entity to be created.
     * @param team the team of the player.
     */
    void createPlayer(Player player, Team team);

    /**
     * Returns all players that currently are not in any team.
     * @return List of players without a team.
     */
    Collection<Player> getFreePlayers();

    /**
     * Get all players.
     *
     * @return Collection of all players in the league
     */
    Collection<Player> getAllPlayers();

    /**
     * Adds the player to a team and also adds the reference to
     * that team to the player.
     * @param player The player to be updated.
     * @param team new team of the updated player.
     */
    void addTeamToPlayer(Player player, Team team);

    /**
     * Removes the player from a team and also sets the player
     * team reference to null.
     * @param player The player to be removed.
     * @param team The team from which the player is removed.
     */
    void removeTeamFromPlayer(Player player, Team team);

}
