package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.config.PersistanceApplicationConfig;
import cz.muni.fi.pa165.icehockeymanager.dao.TeamDao;
import cz.muni.fi.pa165.icehockeymanager.exceptions.PlayerNotInTeamException;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownGameException;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistanceApplicationConfig.class)
class TeamServiceImplTest {

    private long idCounter;

    @Mock
    private TeamDao teamDao;

    @InjectMocks
    private TeamServiceImpl teamService;

    @Test
    public void getEmptyAllTeams() {
        when(teamDao.readAll()).thenReturn(new ArrayList<>());
        assertThat(teamService.getAllTeams()).isEmpty();
        verify(teamDao).readAll();
    }

    @Test
    public void getAllTeams() {
        var expected = buildTeams();
        when(teamDao.readAll()).thenReturn(expected);
        assertThat(teamService.getAllTeams()).hasSameElementsAs(expected);
        verify(teamDao).readAll();
    }

    @Test
    public void findNonexistentTeam() {
        when(teamDao.read(1L)).thenReturn(Optional.empty());
        assertThat(teamService.findTeam(1)).isEmpty();
        verify(teamDao).read(1L);
    }

    @Test
    public void findExistingTeam() {
        var expected = Optional.of(buildTeam("Carolina Hurricanes"));
        when(teamDao.read(1L)).thenReturn(expected);
        assertThat(teamService.findTeam(1)).isEqualTo(expected);
        verify(teamDao).read(1L);
    }

    @Test
    public void addPlayerToTeamNewPlayer() {
        Team team = buildTeam("Florida Panthers");
        var players = buildPlayers();
        team.setPlayers(players);
        Player player = buildPlayer(3L, "Miroslav Satan");
        players = new HashSet<>(players);
        players.add(player);
        teamService.addPlayerToTeam(team, player);
        assertThat(team.getPlayers()).hasSameElementsAs(players);
        verify(teamDao).update(team);
    }

    @Test
    public void addPlayerToTeamPlayerIsNull() {
        Team team = buildTeam("Florida Panthers");
        var players = buildPlayers();
        team.setPlayers(players);
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
            () -> teamService.addPlayerToTeam(team, null)
        );
        assertThat(team.getPlayers()).hasSameElementsAs(players);
    }

    @Test
    public void addPlayerToTeamTeamIsNull() {
        var player = buildPlayer(1L, "Stan Mikita");
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamService.addPlayerToTeam(null,player)
        );
    }

    @Test
    public void removePlayerFromTeam() {
        Team team = buildTeam("Florida Panthers");
        var players = buildPlayers();
        Player player = buildPlayer(3L, "Miroslav Satan");
        var expected = new HashSet<>(players);
        players.add(player);
        team.setPlayers(players);
        teamService.removePlayerFromTeam(team, player);
        assertThat(team.getPlayers()).hasSameElementsAs(expected);
        verify(teamDao).update(team);
    }

    @Test
    public void removePlayerFromTeamPlayerIsNotInTeam() {
        Team team = buildTeam("Florida Panthers");
        var players = buildPlayers();
        Player player = buildPlayer(3L, "Miroslav Satan");
        team.setPlayers(players);
        assertThatExceptionOfType(PlayerNotInTeamException.class).isThrownBy(
                () -> teamService.removePlayerFromTeam(team, player)
        );
        assertThat(team.getPlayers()).hasSameElementsAs(players);
    }

    @Test
    public void removePlayerFromTeamPlayerIsNull() {
        var team = buildTeam("Dallas Stars");
        assertThatExceptionOfType(PlayerNotInTeamException.class).isThrownBy(
                () -> teamService.removePlayerFromTeam(team, null)
        );
    }

    @Test
    public void removePlayerFromTeamTeamIsNull() {
        var player = buildPlayer(1L, "Peter Bondra");
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamService.removePlayerFromTeam(null, player)
        );
    }

    @Test
    public void addHomeGame() {
        Team team = buildTeam("Florida Panthers");
        Game game = buildGame();
        teamService.addHomeGame(team, game);
        assertThat(team.getHomeGames()).containsOnly(game);
        verify(teamDao).update(team);
    }

    @Test
    public void addHomeGameGameIsNull() {
        Team team = buildTeam("Florida Panthers");
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamService.addHomeGame(team, null)
        );
        assertThat(team.getHomeGames()).isEmpty();
    }

    @Test
    public void addHomeGameTeamIsNull() {
        Game game = buildGame();
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamService.addHomeGame(null, game)
        );
    }

    @Test
    public void addAwayGame() {
        Team team = buildTeam("Florida Panthers");
        Game game = buildGame();
        teamService.addAwayGame(team, game);
        assertThat(team.getAwayGames()).containsOnly(game);
        verify(teamDao).update(team);
    }

    @Test
    public void addAwayGameGameIsNull() {
        Team team = buildTeam("Florida Panthers");
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamService.addAwayGame(team, null)
        );
        assertThat(team.getAwayGames()).isEmpty();
    }

    @Test
    public void addAwayGameTeamIsNull() {
        Game game = buildGame();
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamService.addAwayGame(null, game)
        );
    }

    @Test
    public void removeHomeGameOneOfOne() {
        Team team = buildTeam("Florida Panthers");
        Game game = buildGame();
        team.setHomeGames(Set.of(game));
        teamService.removeHomeGame(team, game);
        assertThat(team.getHomeGames()).isEmpty();
        verify(teamDao).update(team);
    }

    @Test
    public void removeHomeGameOneOfMany() {
        Team team = buildTeam("Florida Panthers");
        Game game = buildGame();
        game.setId(2L);
        Game game2 = buildGame();
        team.setHomeGames(Set.of(game, game2));
        teamService.removeHomeGame(team, game);
        assertThat(team.getHomeGames()).containsOnly(game2);
        verify(teamDao).update(team);
    }

    @Test
    public void removeHomeGameGameIsNull() {
        Team team = buildTeam("Florida Panthers");
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamService.addHomeGame(team, null)
        );
        assertThat(team.getHomeGames()).isEmpty();
    }

    @Test
    public void removeHomeGameTeamIsNull() {
        Game game = buildGame();
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamService.addHomeGame(null, game)
        );
    }

    @Test
    public void removeHomeGameNotPresent() {
        Team team = buildTeam("Florida Panthers");
        Game game = buildGame();
        game.setId(2L);
        Game game2 = buildGame();
        team.setHomeGames(Set.of(game));
        assertThatExceptionOfType(UnknownGameException.class).isThrownBy(
                () -> teamService.removeHomeGame(team, game2)
        );
        assertThat(team.getHomeGames()).containsOnly(game);
    }

    @Test
    public void removeAwayGameOneOfOne() {
        Team team = buildTeam("Florida Panthers");
        Game game = buildGame();
        team.setAwayGames(Set.of(game));
        teamService.removeAwayGame(team, game);
        assertThat(team.getAwayGames()).isEmpty();
        verify(teamDao).update(team);
    }

    @Test
    public void removeAwayGameOneOfMany() {
        Team team = buildTeam("Florida Panthers");
        Game game = buildGame();
        game.setId(2L);
        Game game2 = buildGame();
        team.setAwayGames(Set.of(game, game2));
        teamService.removeAwayGame(team, game);
        assertThat(team.getAwayGames()).containsOnly(game2);
        verify(teamDao).update(team);
    }

    @Test
    public void removeAwayGameGameIsNull() {
        Team team = buildTeam("Florida Panthers");
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamService.addAwayGame(team, null)
        );
        assertThat(team.getAwayGames()).isEmpty();
    }

    @Test
    public void removeAwayGameTeamIsNull() {
        Game game = buildGame();
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamService.addAwayGame(null, game)
        );
    }

    @Test
    public void removeAwayGameNotPresent() {
        Team team = buildTeam("Florida Panthers");
        Game game = buildGame();
        game.setId(2L);
        Game game2 = buildGame();
        team.setAwayGames(Set.of(game));
        assertThatExceptionOfType(UnknownGameException.class).isThrownBy(
                () -> teamService.removeAwayGame(team, game2)
        );
        assertThat(team.getAwayGames()).containsOnly(game);
    }

    @Test
    public void createTeam() {
        Team team = buildTeam("Florida Panthers");
        teamService.createTeam(team);
        verify(teamDao).create(team);
    }

    private Game buildGame() {
        var game = new Game();
        game.setId(1L);
        return game;
    }

    private Team buildTeam(String name) {
        var team = new Team();
        team.setName(name);
        idCounter++;
        team.setId(idCounter);
        return team;
    }

    private List<Team> buildTeams() {
        var teams = new ArrayList<Team>();
        teams.add(buildTeam("Carolina Hurricanes"));
        teams.add(buildTeam("Colorado Avalanche"));
        return teams;
    }

    private Player buildPlayer(Long id, String name) {
        var player = new Player();
        player.setId(id);
        player.setName(name);
        return player;
    }

    private Set<Player> buildPlayers() {
        var players = new HashSet<Player>();
        players.add(buildPlayer(1L, "Marian Hossa"));
        players.add(buildPlayer(2L, "Dominik Hasek"));
        return players;
    }
}