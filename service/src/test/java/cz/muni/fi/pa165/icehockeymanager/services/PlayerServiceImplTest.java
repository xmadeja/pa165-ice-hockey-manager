package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.config.PersistanceApplicationConfig;
import cz.muni.fi.pa165.icehockeymanager.dao.PlayerDao;
import cz.muni.fi.pa165.icehockeymanager.exceptions.PlayerNotInTeamException;
import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistanceApplicationConfig.class)
class PlayerServiceImplTest {
    @Mock
    private PlayerDao playerDao;

    @InjectMocks
    private PlayerServiceImpl playerService;

    @Test
    public void findNonExistingPlayer() {
        when(playerDao.read(1L)).thenReturn(Optional.empty());
        assertThat(playerService.findPlayer(1L)).isEmpty();
        verify(playerDao).read(1L);
    }

    @Test
    public void findExistingPlayer() {
        var player = buildPlayerMock(1L, "Sidney Crosby");
        var expected = Optional.of(player);
        when(playerDao.read(1L)).thenReturn(expected);
        assertThat(playerService.findPlayer(1L)).isSameAs(expected);
        verify(playerDao).read(1L);
    }

    @Test
    public void createPlayer() {
        Player player = buildPlayer(1L, "Jaromir Jagr");
        Team team = buildTeam(1L, "Florida Panthers");
        playerService.createPlayer(player, team);
        assertThat(player.getTeam().orElse(null)).isSameAs(team);
        verify(playerDao).create(player);
    }

    @Test
    public void getFreePlayersEmpty() {
        when(playerDao.getAllFreePlayers()).thenReturn(new ArrayList<>());
        assertThat(playerService.getFreePlayers()).isEmpty();
        verify(playerDao).getAllFreePlayers();
    }

    @Test
    public void getFreePlayersNonEmpty() {
        var players = Arrays.asList(
                buildPlayerMock(1L, "Zdeno Chara"),
                buildPlayerMock(2L, "David Pastrnak")
        );
        when(playerDao.getAllFreePlayers()).thenReturn(players);
        assertThat(playerService.getFreePlayers()).hasSameElementsAs(players);
        verify(playerDao).getAllFreePlayers();
    }

    @Test
    public void addTeamToPlayer() {
        Player player = buildPlayer(1L, "Jaromir Jagr");
        Team team = buildTeam(1L, "Florida Panthers");
        playerService.addTeamToPlayer(player, team);
        assertThat(player.getTeam().orElse(null)).isSameAs(team);
        verify(playerDao).update(player);
    }


    @Test
    public void removeTeamFromPlayer() {
        Player player = buildPlayer(1L, "Jaromir Jagr");
        Team team = buildTeam(1L, "Florida Panthers");
        player.setTeam(team);
        playerService.removeTeamFromPlayer(player, team);
        assertThat(player.getTeam()).isEmpty();
        verify(playerDao).update(player);
    }

    @Test
    public void removeTeamFromPlayerTeamIsNull() {
        Player player = buildPlayer(1L, "Jaromir Jagr");
        assertThatExceptionOfType(PlayerNotInTeamException.class).isThrownBy(
                () -> playerService.removeTeamFromPlayer(player, null)
        );
    }

    @Test
    public void removeTeamFromPlayerTeamIsDifferent() {
        Player player = buildPlayer(1L, "Jaromir Jagr");
        Team team = buildTeam(1L, "Florida Panthers");
        Team team2 = buildTeam(2L, "Colorado Avalanches");
        player.setTeam(team);
        assertThatExceptionOfType(PlayerNotInTeamException.class).isThrownBy(
                () -> playerService.removeTeamFromPlayer(player, team2)
        );
    }

    private Player buildPlayerMock(Long id, String name) {
        var player = mock(Player.class);
        when(player.getId()).thenReturn(id);
        when(player.getName()).thenReturn(name);
        return player;
    }

    private Player buildPlayer(Long id, String name) {
        var player = new Player();
        player.setId(id);
        player.setName(name);
        return player;
    }

    private Team buildTeam(Long id, String name) {
        var team = new Team();
        team.setId(id);
        team.setName(name);
        return team;
    }
}