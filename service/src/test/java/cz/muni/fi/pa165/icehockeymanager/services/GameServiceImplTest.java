package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.config.PersistanceApplicationConfig;
import cz.muni.fi.pa165.icehockeymanager.dao.GameDao;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownTeamException;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistanceApplicationConfig.class)
class GameServiceImplTest {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm z");

    @Mock
    private GameDao gameDao;

    @InjectMocks
    private GameServiceImpl gameService;

    @Test
    public void getAllGamesNoGames() {
        when(gameDao.readAll()).thenReturn(new ArrayList<>());
        assertThat(gameService.getAllGames()).isEmpty();
    }

    @Test
    public void getAllGamesFoundGames() {
        var teamOne = buildTeam(1L, "Vancouver Canucks");
        var teamTwo = buildTeam(2L, "Calgary Flames");
        List<Game> expected = Arrays.asList(
                buildGame(1L, teamOne, teamTwo, teamOne, 2, 1, "2021-01-01 16:00 GMT"),
                buildGame(1L, teamTwo, teamOne, teamOne, 1, 2, "2021-02-01 16:00 GMT")
        );
        when(gameDao.readAll()).thenReturn(expected);
        assertThat(gameService.getAllGames()).containsExactlyElementsOf(expected);
    }

    @Test
    public void createGame() {
        Game game = buildGame();
        gameService.createGame(game);
        verify(gameDao).create(game);
    }

    @Test
    public void findNonexistentGame() {
        when(gameDao.read(1L)).thenReturn(Optional.empty());
        assertThat(gameService.findGame(1L)).isEmpty();
        verify(gameDao).read(1L);
    }

    @Test
    public void findExistingGame() {
        var expected = Optional.of(buildGame());
        when(gameDao.read(1L)).thenReturn(expected);
        assertThat(gameService.findGame(1L)).isEqualTo(expected);
        verify(gameDao).read(1L);
    }

    @Test
    public void deleteGame() {
        Game game = buildGame();
        gameService.deleteGame(game);
        verify(gameDao).delete(game);
    }

    @Test
    public void updateGameScoreHomeWinning() {
        Game game = buildGameWithTeams();
        gameService.updateGameScore(game, 1, 0);
        assertThat(game.getHomeTeamScore()).isEqualTo(1);
        assertThat(game.getAwayTeamScore()).isEqualTo(0);
        assertThat(game.getWinner().orElse(null)).isEqualTo(game.getHomeTeam());
        verify(gameDao).update(game);
    }

    @Test
    public void updateGameScoreAwayWinning() {
        Game game = buildGameWithTeams();
        gameService.updateGameScore(game, 1, 2);
        assertThat(game.getHomeTeamScore()).isEqualTo(1);
        assertThat(game.getAwayTeamScore()).isEqualTo(2);
        assertThat(game.getWinner().orElse(null)).isEqualTo(game.getAwayTeam());
        verify(gameDao).update(game);
    }

    @Test
    public void updateGameScoreDraw() {
        Game game = buildGameWithTeams();
        gameService.updateGameScore(game, 100, 100);
        assertThat(game.getHomeTeamScore()).isEqualTo(100);
        assertThat(game.getAwayTeamScore()).isEqualTo(100);
        assertThat(game.getWinner()).isEmpty();
        verify(gameDao).update(game);
    }

    @Test
    public void setGameWinner() {
        Game game = buildGame();
        Team team = buildTeam("CSKA Moskva");
        game.setHomeTeam(team);
        gameService.setGameWinner(game, team);
        assertThat(game.getWinner().orElse(null)).isEqualTo(team);
        verify(gameDao).update(game);
    }

    @Test
    public void setGameWinnerNull() {
        Game game = buildGame();
        gameService.setGameWinner(game, null);
        assertThat(game.getWinner()).isEmpty();
        verify(gameDao).update(game);
    }

    @Test
    public void setGameWinnerUnknownTeam() {
        Game game = buildGame();
        Team team = buildTeam("CSKA Moskva");
        assertThatExceptionOfType(UnknownTeamException.class).isThrownBy(
                () -> gameService.setGameWinner(game, team)
        );
        assertThat(game.getWinner()).isEmpty();
    }


    private Team buildTeam(String name) {
        return buildTeam(1L, name);
    }

    private Team buildTeam(long id, String name) {
        var team = new Team();
        team.setId(id);
        team.setName(name);
        return team;
    }

    private Game buildGameWithTeams() {
        var teamOne = buildTeam(1L, "Vancouver Canucks");
        var teamTwo = buildTeam(2L, "Calgary Flames");
        return buildGame(1L, teamTwo, teamOne, null, 1, 2, "2021-02-01 16:00 GMT");
    }
    private Game buildGame() {
        var game = new Game();
        game.setId(1L);
        return game;
    }

    private Game buildGame(
            Long id,
            Team homeTeam,
            Team awayTeam,
            Team winner,
            int homeTeamScore,
            int awayTeamScore,
            String gameDateTime
    ) {
        var game = new Game();
        game.setId(id);
        game.setHomeTeam(homeTeam);
        game.setAwayTeam(awayTeam);
        game.setWinner(winner);
        game.setHomeTeamScore(homeTeamScore);
        game.setAwayTeamScore(awayTeamScore);
        game.setGameDateTime(ZonedDateTime.parse(gameDateTime, formatter));
        return game;
    }
}