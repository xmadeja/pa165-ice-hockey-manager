package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.config.PersistanceApplicationConfig;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerCreateDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerTransferDto;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownPlayerException;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownTeamException;
import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import cz.muni.fi.pa165.icehockeymanager.services.BeanMappingService;
import cz.muni.fi.pa165.icehockeymanager.services.BeanMappingServiceImpl;
import cz.muni.fi.pa165.icehockeymanager.services.PlayerService;
import cz.muni.fi.pa165.icehockeymanager.services.TeamService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistanceApplicationConfig.class)
public class TeamManagerFacadeImplTest {

    @Autowired
    private ApplicationContext context;

    private PlayerService playerService;

    private TeamService teamService;

    private BeanMappingService beanMappingService;

    private TeamManagerFacade teamManagerFacade;

    @BeforeEach
    public void setUp() {
        playerService = mock(PlayerService.class);
        teamService = mock(TeamService.class);
        beanMappingService = context.getBean(BeanMappingServiceImpl.class);
        teamManagerFacade = new TeamManagerFacadeImpl(playerService, teamService, beanMappingService);
    }

    @Test
    public void getFreePlayersEmpty() {
        when(playerService.getFreePlayers()).thenReturn(new ArrayList<>());
        assertThat(teamManagerFacade.getFreePlayers()).isEmpty();
        verify(playerService).getFreePlayers();
    }

    @Test
    public void getFreePlayersNonEmpty() {
        var players = Arrays.asList(
                buildPlayerMock(1L, "Zdeno Chara"),
                buildPlayerMock(2L, "David Pastrnak")
        );
        var expected = beanMappingService.mapTo(players, PlayerDto.class);
        when(playerService.getFreePlayers()).thenReturn(players);
        assertThat(teamManagerFacade.getFreePlayers()).hasSameElementsAs(expected);
        verify(playerService).getFreePlayers();
    }

    @Test
    public void firePlayerNotInTeam() {
        Player player = buildPlayerMock(1L, "Andrej Sekera");
        when(playerService.findPlayer(1L)).thenReturn(Optional.of(player));
        assertThatExceptionOfType(UnknownTeamException.class).isThrownBy(
                () -> teamManagerFacade.firePlayer(1L)
        );
    }

    @Test
    public void firePlayerNonexistentPlayer() {
        Team team = buildTeamMock(1L, "Boston Bruins");
        when(teamService.findTeam(1L)).thenReturn(Optional.ofNullable(team));
        when(playerService.findPlayer(1L)).thenReturn(Optional.empty());
        assertThatExceptionOfType(UnknownPlayerException.class).isThrownBy(
                () -> teamManagerFacade.firePlayer(1L)
        );
        verify(playerService).findPlayer(1L);
    }

    @Test
    public void firePlayer() {
        Team team = buildTeamMock(1L, "Boston Bruins");
        when(teamService.findTeam(1L)).thenReturn(Optional.ofNullable(team));
        Player player = buildPlayerMock(1L, "David Pastrnak", team);
        when(playerService.findPlayer(1L)).thenReturn(Optional.of(player));
        teamManagerFacade.firePlayer(1L);
        verify(teamService).removePlayerFromTeam(team, player);
        verify(playerService).removeTeamFromPlayer(player, team);
    }

    @Test
    public void recruitVeteranPlayerNonexistentTeam() {
        when(teamService.findTeam(1L)).thenReturn(Optional.empty());
        PlayerTransferDto player = new PlayerTransferDto();
        player.setPlayerId(1L);
        player.setTeamId(1L);
        assertThatExceptionOfType(UnknownTeamException.class).isThrownBy(
                () -> teamManagerFacade.recruitPlayer(player)
        );
        verify(teamService).findTeam(1L);
    }

    @Test
    public void recruitVeteranPlayerNonexistentPlayer() {
        Team team = buildTeamMock(1L, "Boston Bruins");
        PlayerTransferDto player = new PlayerTransferDto();
        player.setPlayerId(1L);
        player.setTeamId(1L);
        when(teamService.findTeam(1L)).thenReturn(Optional.ofNullable(team));
        when(playerService.findPlayer(1L)).thenReturn(Optional.empty());
        assertThatExceptionOfType(UnknownPlayerException.class).isThrownBy(
                () -> teamManagerFacade.recruitPlayer(player)
        );
        verify(teamService).findTeam(1L);
    }

    @Test
    public void recruitNewPlayerNonexistentTeam() {
        PlayerCreateDto player = buildPlayerCreateDtoMock("Zdeno Chara");
        when(teamService.findTeam(1L)).thenReturn(Optional.empty());
        assertThatExceptionOfType(UnknownTeamException.class).isThrownBy(
                () -> teamManagerFacade.recruitNewPlayer(player)
        );
        verify(teamService).findTeam(1L);
    }

    public Team buildTeamMock(Long id, String name) {
        var team = mock(Team.class);
        when(team.getId()).thenReturn(id);
        when(team.getName()).thenReturn(name);
        return team;
    }

    private Player buildPlayerMock(Long id, String name) {
        var player = mock(Player.class);
        when(player.getId()).thenReturn(id);
        when(player.getName()).thenReturn(name);
        return player;
    }

    private Player buildPlayerMock(Long id, String name, Team team) {
        var player = mock(Player.class);
        when(player.getId()).thenReturn(id);
        when(player.getName()).thenReturn(name);
        when(player.getTeam()).thenReturn(Optional.ofNullable(team));
        return player;
    }

    private PlayerCreateDto buildPlayerCreateDtoMock(String name) {
        var player = new PlayerCreateDto();
        player.setName(name);
        player.setTeamId(1L);
        return player;
    }
}
