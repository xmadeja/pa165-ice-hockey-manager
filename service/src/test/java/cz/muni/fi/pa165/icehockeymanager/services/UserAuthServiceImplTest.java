package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.config.ApplicationConfig;
import cz.muni.fi.pa165.icehockeymanager.dao.UserAuthDao;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UsernameTakenException;
import cz.muni.fi.pa165.icehockeymanager.model.Account;
import cz.muni.fi.pa165.icehockeymanager.security.Roles;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.InjectSoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityExistsException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


@ExtendWith(SpringExtension.class)
@ExtendWith(SoftAssertionsExtension.class)
@ContextConfiguration(classes = ApplicationConfig.class)
class UserAuthServiceImplTest {

    public static final String CORRECT_USERNAME = "ivan";
    public static final String INCORRECT_USERNAME = "gzegorz";
    public static final String NEW_USERNAME = "fedor";

    public static final String CORRECT_PASSWORD = "correct";
    public static final String INCORRECT_PASSWORD = "incorrect";
    public static final String NEW_PASSWORD = "new_password";

    private List<Account> users = new ArrayList<>();

    private Account user1;

    private UserAuthService authService;

    @InjectSoftAssertions
    private SoftAssertions softly;

    @Autowired
    private Argon2Service argonEncoder;

    private UserAuthDao userAuthDaoMock;

    @BeforeEach
    public void setUp() {
        userAuthDaoMock = mock(UserAuthDao.class);
        authService = new UserAuthServiceImpl(argonEncoder, userAuthDaoMock);

        buildMockDB();
    }

    @Test
    public void authenticateCorrectPassword() {
        softly.assertThat(
                authService.authenticateUser(CORRECT_USERNAME, CORRECT_PASSWORD)
        ).hasValue(user1);
    }

    @Test
    public void authenticateIncorrectPassword() {
        softly.assertThat(
                authService.authenticateUser(CORRECT_USERNAME, INCORRECT_PASSWORD)
        ).isEmpty();
    }

    @Test
    public void authenticateIncorrectUsername() {
        softly.assertThat(
                authService.authenticateUser(INCORRECT_USERNAME, CORRECT_PASSWORD)
        ).isEmpty();
    }

    @Test
    public void createNewLeagueManager() {
        ArgumentCaptor<Account> valueCapture = ArgumentCaptor.forClass(Account.class);
        doAnswer(
                (invocation) -> users.add(invocation.getArgument(0))
        ).when(userAuthDaoMock).create(valueCapture.capture());

        var user = authService.createLeagueManager(NEW_USERNAME, NEW_PASSWORD);
        softly.assertThat(user).isEqualTo(valueCapture.getValue());
        softly.assertThat(user.getRole()).isEqualTo(roleToString(Roles.LEAGUE_MANAGER));

        var authenticatedUser = authService.authenticateUser(NEW_USERNAME, NEW_PASSWORD);
        assertThat(authenticatedUser).hasValue(user);
        assertThat(authenticatedUser.orElse(null).getRole()).isEqualTo(roleToString(Roles.LEAGUE_MANAGER));
    }

    @Test
    public void createNewLeagueManagerUsernameExists() {
        ArgumentCaptor<Account> valueCapture = ArgumentCaptor.forClass(Account.class);
        doAnswer(
                (invocation) -> {
                    throw new EntityExistsException();
                }
        ).when(userAuthDaoMock).create(valueCapture.capture());

        assertThatExceptionOfType(UsernameTakenException.class)
                .isThrownBy(() -> authService.createLeagueManager(NEW_USERNAME, NEW_PASSWORD));
    }

    @Test
    public void createNewTeamManager() {
        ArgumentCaptor<Account> valueCapture = ArgumentCaptor.forClass(Account.class);
        doAnswer(
                (invocation) -> users.add(invocation.getArgument(0))
        ).when(userAuthDaoMock).create(valueCapture.capture());

        var user = authService.createTeamManager(NEW_USERNAME, NEW_PASSWORD);
        softly.assertThat(user).isEqualTo(valueCapture.getValue());
        softly.assertThat(user.getRole()).isEqualTo(roleToString(Roles.TEAM_MANAGER));

        var authenticatedUser = authService.authenticateUser(NEW_USERNAME, NEW_PASSWORD);
        assertThat(authenticatedUser).hasValue(user);
        assertThat(authenticatedUser.orElse(null).getRole()).isEqualTo(roleToString(Roles.TEAM_MANAGER));
    }

    @Test
    public void createNewTeamManagerUsernameExists() {
        ArgumentCaptor<Account> valueCapture = ArgumentCaptor.forClass(Account.class);
        doAnswer(
                (invocation) -> {
                    throw new EntityExistsException();
                }
        ).when(userAuthDaoMock).create(valueCapture.capture());

        assertThatExceptionOfType(UsernameTakenException.class)
                .isThrownBy(() -> authService.createTeamManager(NEW_USERNAME, NEW_PASSWORD));
    }

    private static String roleToString(Roles role) {
        return role.toString();
    }

    private void buildMockDB() {
        String password = argonEncoder.encodePassword(CORRECT_PASSWORD);
        user1 = buildUser(1L, CORRECT_USERNAME, password);

        users.add(user1);

        when(userAuthDaoMock.findByUsername(anyString()))
                .thenAnswer(
                        invocation -> {
                            String username = invocation.getArgument(0);
                            for (var user: users) {
                                if (username.equals(user.getUsername())) {
                                    return Optional.of(user);
                                }
                            }
                            return Optional.empty();
                        });
    }

    private static Account buildUser(Long id, String username, String passwordHash) {
        var user = mock(Account.class);
        when(user.getId()).thenReturn(id);
        when(user.getUsername()).thenReturn(username);
        when(user.getPassword()).thenReturn(passwordHash);
        return user;
    }
}