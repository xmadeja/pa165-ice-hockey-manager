package cz.muni.fi.pa165.icehockeymanager.services;

import cz.muni.fi.pa165.icehockeymanager.config.ApplicationConfig;
import org.assertj.core.api.SoftAssertions;
import org.assertj.core.api.junit.jupiter.InjectSoftAssertions;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ExtendWith(SoftAssertionsExtension.class)
@ContextConfiguration(classes = ApplicationConfig.class)
class Argon2ServiceImplTest {

    @InjectSoftAssertions
    SoftAssertions softly;

    public static final String CORRECT_PASSWORD = "correct";
    public static final String INCORRECT_PASSWORD = "incorrect";

    @Autowired
    Argon2Service authService;

    @Test
    public void correctPassword() {
        String encoded = authService.encodePassword(CORRECT_PASSWORD);
        softly.assertThat(encoded).isNotEmpty();
        softly.assertThat(authService.compare(CORRECT_PASSWORD, encoded)).isTrue();
    }

    @Test
    public void incorrectPassword() {
        String encoded = authService.encodePassword(CORRECT_PASSWORD);
        softly.assertThat(encoded).isNotEmpty();
        softly.assertThat(authService.compare(INCORRECT_PASSWORD, encoded)).isFalse();
    }
}