package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.config.PersistanceApplicationConfig;
import cz.muni.fi.pa165.icehockeymanager.dto.GameDto;
import cz.muni.fi.pa165.icehockeymanager.dto.PlayerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.TeamDto;
import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownTeamException;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import cz.muni.fi.pa165.icehockeymanager.services.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistanceApplicationConfig.class)
class UserFacadeImplTest {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm z");

    @Autowired
    private ApplicationContext context;

    private PlayerService playerService;

    private TeamService teamService;

    private GameService gameService;

    private BeanMappingService beanMappingService;

    private UserFacade userFacade;
    private Team teamPenguinsMock;
    private Team teamBruinsMock;
    private List<Game> gameMocks;
    private TeamDto teamPenguinsDTO;
    private TeamDto teamBruinsDTO;
    private List<GameDto> gameDtos;

    @BeforeEach
    public void setUp() {
        playerService = mock(PlayerService.class);
        teamService = mock(TeamService.class);
        gameService = mock(GameService.class);
        beanMappingService = context.getBean(BeanMappingServiceImpl.class);
        userFacade = new UserFacadeImpl(playerService, teamService, gameService, beanMappingService);
    }

    @Test
    public void getNonexistentPlayer() {
        when(playerService.findPlayer(1)).thenReturn(Optional.empty());
        assertThat(userFacade.findPlayer(1)).isEmpty();
        verify(playerService).findPlayer(1);
    }

    @Test
    public void getExistingPlayer() {
        var player = mock(Player.class);
        when(player.getId()).thenReturn(1L);
        when(player.getName()).thenReturn("Satan");
        when(playerService.findPlayer(1)).thenReturn(Optional.of(player));

        var expected = new PlayerDto();
        expected.setId(1L);
        expected.setName("Satan");

        Optional<PlayerDto> result = userFacade.findPlayer(1);
        Assertions.assertThat(result).isEqualTo(Optional.of(expected));
        verify(playerService).findPlayer(1);
    }

    @Test
    public void getTeamsInLeagueEmpty() {
        when(teamService.getAllTeams()).thenReturn(new ArrayList<>());
        assertThat(userFacade.getTeamsInLeague()).isEmpty();
        verify(teamService).getAllTeams();
    }

    @Test
    public void getTeamsInLeagueNonEmpty() {
        var teams = Arrays.asList(
                buildTeamMock(1L, "Pittsburgh Penguins"),
                buildTeamMock(2L, "Chicago Blackhawks"),
                buildTeamMock(3L, "Florida Panthers")
        );
        var expected = beanMappingService.mapTo(teams, TeamDto.class);
        when(teamService.getAllTeams()).thenReturn(teams);
        assertThat(userFacade.getTeamsInLeague()).hasSameElementsAs(expected);
        verify(teamService).getAllTeams();
    }

    @Test
    public void getPlayersInTeamNoTeam() {
        when(teamService.findTeam(1)).thenReturn(Optional.empty());
        assertThatExceptionOfType(UnknownTeamException.class).isThrownBy(
                () -> userFacade.getPlayersInTeam(1)
        );
        verify(teamService).findTeam(1);
    }

    @Test
    public void getPlayersInTeamNoPlayers() {
        var team = buildTeamMock(1L, "Pittsburgh Penguins");
        when(team.getPlayers()).thenReturn(new HashSet<>());
        when(teamService.findTeam(1)).thenReturn(Optional.of(team));

        assertThat(userFacade.getPlayersInTeam(1)).isEmpty();
        verify(teamService).findTeam(1);
    }

    @Test
    public void getPlayersInTeamSomePlayers() {
        var team = addPlayers(
                buildTeamMock(1L, "Pittsburgh Penguins"),
                buildPlayerMock(1L, "Wayne Gretzky"),
                buildPlayerMock(2L, "Gordie Howe"),
                buildPlayerMock(3L, "Sidney Crosby")
        );
        when(teamService.findTeam(1)).thenReturn(Optional.of(team));

        assertThat(userFacade.getPlayersInTeam(1)).hasSameElementsAs(
                asCollection(
                        buildPlayerDTO(1L, "Wayne Gretzky"),
                        buildPlayerDTO(2L, "Gordie Howe"),
                        buildPlayerDTO(3L, "Sidney Crosby")
                )
        );
        verify(teamService).findTeam(1);
    }

    @Test
    public void getGamesForTeamSomeGames() {
        buildTeamsAndGames();
        when(teamService.findTeam(1)).thenReturn(Optional.of(teamPenguinsMock));
        assertThat(userFacade.getGamesForTeam(1)).hasSameElementsAs(
                gameDtos
        );
        verify(teamService).findTeam(1);
    }

    @Test
    public void getGamesForNonexistentTeam() {
        when(teamService.findTeam(1)).thenReturn(Optional.empty());
        assertThatExceptionOfType(UnknownTeamException.class).isThrownBy(
                () -> userFacade.getGamesForTeam(1)
        );
        verify(teamService).findTeam(1);
    }

    @Test
    public void getAllGamesInLeagueNoGames() {
        when(gameService.getAllGames()).thenReturn(new ArrayList<>());
        assertThat(userFacade.getGamesForLeague()).isEmpty();
    }

    @Test
    public void getAllGamesInLeagueFoundGames() {
        buildTeamsAndGames();
        when(gameService.getAllGames()).thenReturn(gameMocks);
        assertThat(userFacade.getGamesForLeague()).containsExactlyElementsOf(gameDtos);
    }

    public Player buildPlayerMock(Long id, String name) {
        var player = mock(Player.class);
        when(player.getId()).thenReturn(id);
        when(player.getName()).thenReturn(name);
        return player;
    }

    public Team buildTeamMock(Long id, String name) {
        var team = mock(Team.class);
        when(team.getId()).thenReturn(id);
        when(team.getName()).thenReturn(name);
        return team;
    }

    public Team addPlayers(Team team, Player... players) {
        when(team.getPlayers()).thenReturn(new HashSet<>(Arrays.asList(players)));
        return team;
    }

    private Game buildGameMock(
            Long id,
            Team winner,
            int homeTeamScore,
            int awayTeamScore,
            String gameDateTime
    ) {
        var game = mock(Game.class);
        when(game.getId()).thenReturn(id);
        when(game.getWinner()).thenReturn(Optional.ofNullable(winner));
        when(game.getHomeTeamScore()).thenReturn(homeTeamScore);
        when(game.getAwayTeamScore()).thenReturn(awayTeamScore);

        ZonedDateTime parsedDate = ZonedDateTime.parse(gameDateTime, formatter);
        when(game.getGameDateTime()).thenReturn(parsedDate);

        return game;
    }

    private void addHomeGames(Team team, Game... games) {
        for (Game game : games)
            when(game.getHomeTeam()).thenReturn(team);
        when(team.getHomeGames()).thenReturn(new HashSet<>(Arrays.asList(games)));
    }

    private void addAwayGames(Team team, Game... games) {
        for (Game game : games)
            when(game.getAwayTeam()).thenReturn(team);
        when(team.getAwayGames()).thenReturn(new HashSet<>(Arrays.asList(games)));
    }

    public PlayerDto buildPlayerDTO(Long id, String name) {
        var player = new PlayerDto();
        player.setId(id);
        player.setName(name);
        return player;
    }

    public GameDto buildGameDTO(
            Long id,
            TeamDto homeTeam,
            TeamDto awayTeam,
            TeamDto winner,
            int homeTeamScore,
            int awayTeamScore,
            String gameDateTime
    ) {
        var game = new GameDto();
        game.setId(id);
        game.setHomeTeam(homeTeam);
        game.setAwayTeam(awayTeam);
        game.setWinner(winner);
        game.setAwayTeamScore(awayTeamScore);
        game.setHomeTeamScore(homeTeamScore);
        game.setGameDateTime(ZonedDateTime.parse(gameDateTime, formatter));
        return game;
    }

    public TeamDto buildTeamDTO(Long id, String name) {
        var team = new TeamDto();
        team.setId(id);
        team.setName(name);
        return team;
    }

    public void buildTeamsAndGames() {
        teamPenguinsMock = buildTeamMock(1L, "Pittsburgh Penguins");
        teamBruinsMock = buildTeamMock(2L, "Boston Bruins");

        var gameOne = buildGameMock(1L, teamBruinsMock, 1, 2, "2020-01-01 16:00 GMT");
        var gameTwo = buildGameMock(2L, null, 0, 0, "2020-02-01 16:00 GMT");
        var gameThree = buildGameMock(3L, null, 2, 1, "2020-01-02 16:00 GMT");
        var gameFour = buildGameMock(4L, null, 0, 0, "2020-02-02 16:00 GMT");

        addHomeGames(teamPenguinsMock, gameOne, gameTwo);
        addAwayGames(teamPenguinsMock, gameThree, gameFour);

        addAwayGames(teamBruinsMock, gameOne, gameTwo);
        addHomeGames(teamBruinsMock, gameThree, gameFour);

        teamPenguinsDTO = buildTeamDTO(1L, "Pittsburgh Penguins");
        teamBruinsDTO = buildTeamDTO(2L, "Boston Bruins");

        gameMocks = Arrays.asList(gameOne, gameTwo, gameThree, gameFour);

        gameDtos = Arrays.asList(
                buildGameDTO(
                        1L,
                        teamPenguinsDTO, teamBruinsDTO,
                        teamBruinsDTO,
                        1, 2,
                        "2020-01-01 16:00 GMT"),
                buildGameDTO(
                        2L,
                        teamPenguinsDTO, teamBruinsDTO,
                        null,
                        0, 0,
                        "2020-02-01 16:00 GMT"),
                buildGameDTO(
                        3L,
                        teamBruinsDTO, teamPenguinsDTO,
                        null,
                        2, 1,
                        "2020-01-02 16:00 GMT"),
                buildGameDTO(
                        4L,
                        teamBruinsDTO, teamPenguinsDTO,
                        null,
                        0, 0,
                        "2020-02-02 16:00 GMT")
        );
    }


    @SafeVarargs
    public final <T> Collection<T> asCollection(T... items) {
        return Arrays.asList(items.clone());
    }
}