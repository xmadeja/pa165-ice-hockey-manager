package cz.muni.fi.pa165.icehockeymanager.facades;

import cz.muni.fi.pa165.icehockeymanager.config.PersistanceApplicationConfig;
import cz.muni.fi.pa165.icehockeymanager.dto.GameCreateDto;
import cz.muni.fi.pa165.icehockeymanager.dto.GameUpdateScoreDto;
import cz.muni.fi.pa165.icehockeymanager.dto.GameWinnerDto;
import cz.muni.fi.pa165.icehockeymanager.dto.TeamCreateDto;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import cz.muni.fi.pa165.icehockeymanager.services.BeanMappingService;
import cz.muni.fi.pa165.icehockeymanager.services.BeanMappingServiceImpl;
import cz.muni.fi.pa165.icehockeymanager.services.GameService;
import cz.muni.fi.pa165.icehockeymanager.services.TeamService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistanceApplicationConfig.class)
public class LeagueManagerFacadeImplTest {
    @Autowired
    private ApplicationContext context;
    private GameService gameService;
    private TeamService teamService;
    private BeanMappingService beanMappingService;
    private LeagueManagerFacade leagueManagerFacade;

    @BeforeEach
    public void setUp() {
        gameService = mock(GameService.class);
        teamService = mock(TeamService.class);
        beanMappingService = context.getBean(BeanMappingServiceImpl.class);
        leagueManagerFacade = new LeagueManagerFacadeImpl(teamService, beanMappingService, gameService);
    }

    public Team buildTeamMock(Long id, String name) {
        var team = mock(Team.class);
        when(team.getId()).thenReturn(id);
        when(team.getName()).thenReturn(name);
        return team;
    }

    public Game buildGameMock(Long id) {
        var game = mock(Game.class);
        when(game.getId()).thenReturn(id);
        var edmonton = buildTeamMock(1L, "Edmonton");
        var slovan = buildTeamMock(2L, "Slovan");
        when(game.getAwayTeam()).thenReturn(edmonton);
        when(game.getHomeTeam()).thenReturn(slovan);
        return game;
    }

    public GameCreateDto buildGameCreateDTO(Team homeTeam, Team awayTeam) {
        GameCreateDto gameCreateDTO = new GameCreateDto();
        gameCreateDTO.setHomeTeamId(homeTeam.getId());
        gameCreateDTO.setAwayTeamId(awayTeam.getId());
        return gameCreateDTO;
    }

    public GameUpdateScoreDto buildGameUpdateDTO(Game game, int homeScore, int awayScore) {
        var gameUpdate = new GameUpdateScoreDto();
        gameUpdate.setId(game.getId());
        gameUpdate.setAwayTeamScore(awayScore);
        gameUpdate.setHomeTeamScore(homeScore);
        return gameUpdate;
    }

    public GameWinnerDto buildGameWinnerDTO(Game game, long teamId) {
        var gameWinner = new GameWinnerDto();
        gameWinner.setId(game.getId());
        gameWinner.setTeamId(teamId);
        return gameWinner;
    }

    @Test
    public void scheduleGame() {
        var edmonton = buildTeamMock(1L, "Edmonton Oilers");
        var washington = buildTeamMock(2L, "Washington Capitals");
        var gameCreateDTO = buildGameCreateDTO(washington, edmonton);
        when(teamService.findTeam(1L)).thenReturn(Optional.of(edmonton));
        when(teamService.findTeam(2L)).thenReturn(Optional.of(washington));
        leagueManagerFacade.createGame(gameCreateDTO);
        verify(gameService).createGame(any(Game.class));
        verify(teamService).addAwayGame(any(Team.class), any(Game.class));
        verify(teamService).addHomeGame(any(Team.class), any(Game.class));
    }

    @Test
    public void dropGame() {
        var game = buildGameMock(1L);
        when(gameService.findGame(1L)).thenReturn(Optional.ofNullable(game));
        leagueManagerFacade.dropGame(1L);
        verify(gameService).deleteGame(any(Game.class));
        verify(teamService).removeHomeGame(any(Team.class), any(Game.class));
        verify(teamService).removeAwayGame(any(Team.class), any(Game.class));
    }

    @Test
    public void updateGameScore() {
        var game = buildGameMock(1L);
        var gameUpdate = buildGameUpdateDTO(game, 1, 2);
        when(gameService.findGame(1L)).thenReturn(Optional.of(game));
        leagueManagerFacade.updateGameScore(gameUpdate);
        verify(gameService).updateGameScore(game, 1, 2);
    }

    @Test
    public void setGameWinner() {
        var game = buildGameMock(1L);
        when(gameService.findGame(1L)).thenReturn(Optional.ofNullable(game));
        var team = buildTeamMock(1L, "edmonton");
        when(teamService.findTeam(1L)).thenReturn(Optional.ofNullable(team));
        assert game != null;
        var gameWinner = buildGameWinnerDTO(game, 1L);
        leagueManagerFacade.setGameWinner(gameWinner);
        verify(gameService).setGameWinner(game, team);
    }

    @Test
    public void createNewTeam() {
        var teamCreateDTO = new TeamCreateDto();
        leagueManagerFacade.createNewTeam(teamCreateDTO);
        verify(teamService).createTeam(any(Team.class));
    }
}
