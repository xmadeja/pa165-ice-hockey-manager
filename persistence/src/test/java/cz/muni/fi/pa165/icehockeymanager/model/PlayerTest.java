package cz.muni.fi.pa165.icehockeymanager.model;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;

class PlayerTest {

    private Player buildPlayer(Long id, String name, Team team) {
        var player = new Player(name, team);
        player.setId(id);
        return player;
    }

    @Test
    void testIsFree() {
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(buildPlayer(null, "Name", null).isFree())
                    .isTrue();
            softly.assertThat(buildPlayer(null, "Name", new Team()).isFree())
                    .isFalse();
        });
    }

    @Test
    void testEquals() {
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(buildPlayer(null, "Name", null))
                    .isNotEqualTo(buildPlayer(null, "Name", null));
            softly.assertThat(buildPlayer(null, "Name", null))
                    .isNotEqualTo(buildPlayer(null, "Other", null));
            softly.assertThat(buildPlayer(1L, "Name", null))
                    .isEqualTo(buildPlayer(1L, "Name", null));
            softly.assertThat(buildPlayer(1L, "Name", null))
                    .isEqualTo(buildPlayer(1L, "Other", null));
            softly.assertThat(buildPlayer(1L, "Name", null))
                    .isNotEqualTo(buildPlayer(2L, "Name", null));
            var player = buildPlayer(null, "Name", null);
            softly.assertThat(player.equals(player)).isTrue();
            softly.assertThat(player.equals(null)).isFalse();
        });
    }

    @Test
    void testHashCode() {
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(buildPlayer(null, "Name", null))
                    .hasSameHashCodeAs(buildPlayer(null, "Name", null));
            softly.assertThat(buildPlayer(null, "Name", null))
                    .doesNotHaveSameHashCodeAs(buildPlayer(null, "Other", null));
            softly.assertThat(buildPlayer(1L, "Name", null))
                    .hasSameHashCodeAs(buildPlayer(1L, "Name", null));
            softly.assertThat(buildPlayer(1L, "Name", null))
                    .hasSameHashCodeAs(buildPlayer(1L, "Other", null));
            softly.assertThat(buildPlayer(1L, "Name", null))
                    .doesNotHaveSameHashCodeAs(buildPlayer(2L, "Name", null));
        });
    }

    @Test
    void testToString() {
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(buildPlayer(null, "Name", null).toString())
                    .isEqualTo("Player{id=null, name='Name'}");
            softly.assertThat(buildPlayer(1L, "Name", new Team()).toString())
                    .isEqualTo("Player{id=1, name='Name'}");
        });
    }
}