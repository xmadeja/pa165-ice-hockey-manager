package cz.muni.fi.pa165.icehockeymanager.dao;

import cz.muni.fi.pa165.icehockeymanager.config.PersistanceApplicationConfig;
import cz.muni.fi.pa165.icehockeymanager.model.Player;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistanceApplicationConfig.class)
@Transactional
class PlayerDaoTest {

    @Autowired
    private PlayerDao playerDao;

    @Autowired
    private TeamDao teamDao;

    @Autowired
    private EntityManager entityManager;

    private Player buildPlayer() {
        return new Player("Jarda Jagr", null);
    }

    private Team buildTeam() {
        return new Team("Pittsburgh Penguins");
    }

    private List<Player> buildPlayerList() {
        var players = new ArrayList<Player>();
        players.add(new Player("Zdeno Chára", null));
        players.add(new Player("Tomáš Tatar", null));
        players.add(new Player("Dominik Hašek", null));
        players.add(new Player("David Pastrnak", null));
        players.add(new Player("Wayne Gretzky", null));
        return players;
    }

    private Player storeOne(Player player) {
        playerDao.create(player);
        return player;
    }

    private Team storeOne(Team team) {
        teamDao.create(team);
        return team;
    }

    private List<Player> storeAll(List<Player> players) {
        players.stream().forEach(entity -> playerDao.create(entity));
        return players;
    }

    @Test
    public void saveAndGet() {
        var expected = storeOne(buildPlayer());
        var result = playerDao.read(expected.getId()).get();

        assertEntityComparison(result, expected);
    }

    @Test
    public void saveAndGetAllOne() {
        var player = buildPlayer();
        List<Player> expected = new ArrayList<>();
        expected.add(player);

        playerDao.create(player);

        var result = playerDao.readAll();
        assertListComparison(result, expected);
    }

    @Test
    public void saveAndGetAllMultiple() {
        var expected = storeAll(buildPlayerList());

        var result = playerDao.readAll();
        assertListComparison(result, expected);
    }

    @Test
    public void updateOne() {
        var expected = storeOne(buildPlayer());

        expected.setName("Updated Name");
        playerDao.update(expected);

        var result = playerDao.read(expected.getId()).get();
        assertEntityComparison(result, expected);
    }

    @Test
    public void deleteOneOfOne() {
        var player = storeOne(buildPlayer());

        playerDao.delete(player);

        var result = playerDao.readAll();
        assertListComparison(result, new ArrayList<Player>());
    }

    @Test
    public void deleteOneOfMany() {
        var expected = storeAll(buildPlayerList());

        playerDao.delete(expected.get(2));
        expected.remove(2);

        var result = playerDao.readAll();
        assertListComparison(result, expected);
    }

    @Test
    public void saveNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> playerDao.create(null)
        );
    }

    @Test
    public void updateNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> playerDao.update(null)
        );
    }

    @Test
    public void deleteNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> playerDao.delete(null)
        );
    }

    @Test
    public void deleteNonExisting() {
        var entity = buildPlayer();
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> playerDao.delete(entity)
        );
        entityManager.flush();
    }

    @Test
    public void getEmpty() {
        assertThat(playerDao.read(1)).isEmpty();
    }

    @Test
    public void getAllEmpty() {
        assertThat(playerDao.readAll()).isEmpty();
    }

    @Test
    public void getAllFreeAll() {
        var expected = storeAll(buildPlayerList());

        var result = playerDao.getAllFreePlayers();
        assertListComparison(result, expected);
    }

    @Test
    public void getAllFreeSome() {
        var team = storeOne(buildTeam());
        var expected = buildPlayerList();
        expected.get(0).setTeam(team);
        expected.stream().forEach(entity -> playerDao.create(entity));
        expected.remove(0);

        var result = playerDao.getAllFreePlayers();
        assertListComparison(result, expected);
    }

    @Test
    public void getAllFreeEmpty() {
        var result = playerDao.getAllFreePlayers();
        assertThat(result).isNotNull();
        assertThat(result).isEmpty();
    }

    private <T> void assertListComparison(List<T> result, List<T> expected) {
        assertThat(result).isNotNull();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result).hasSameSizeAs(expected);
            softly.assertThat(result).hasSameElementsAs(expected);
        });
    }

    private <T extends Player> void assertEntityComparison(T result, T expected) {
        assertThat(result).isNotNull();
        assertThat(result).isNotNull();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getId()).isEqualTo(expected.getId());
            softly.assertThat(result.getName()).isEqualTo(expected.getName());
            softly.assertThat(result.getTeam()).isEqualTo(expected.getTeam());
            softly.assertThat(result).isEqualTo(expected);
        });
    }
}