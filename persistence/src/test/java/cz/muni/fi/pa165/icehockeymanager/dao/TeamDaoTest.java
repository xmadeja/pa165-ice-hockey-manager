package cz.muni.fi.pa165.icehockeymanager.dao;

import cz.muni.fi.pa165.icehockeymanager.config.PersistanceApplicationConfig;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistanceApplicationConfig.class)
@Transactional
public class TeamDaoTest {

    @Autowired
    private TeamDao teamDao;

    @Autowired
    private EntityManager entityManager;

    private Team buildTeam() {
        return new Team("Washington Capitals");
    }

    private List<Team> buildTeamList() {
        var teams = new ArrayList<Team>();
        teams.add(new Team("Carolina Hurricanes"));
        teams.add(new Team("Colorado Avalanche"));
        teams.add(new Team("Detroit Red Wings"));
        teams.add(new Team("Tampa Bay Lightning"));
        teams.add(new Team("Dallas Stars"));
        return teams;
    }

    private Team storeOne(Team team) {
        teamDao.create(team);
        return team;
    }

    private List<Team> storeAll(List<Team> teams) {
        teams.stream().forEach(entity -> teamDao.create(entity));
        return teams;
    }

    @Test
    public void saveAndGet() {
        var team = buildTeam();
        teamDao.create(team);
        var result = teamDao.read(team.getId()).get();
        Assertions.assertThat(result).isNotNull();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(team.getId()).isEqualTo(result.getId());
            softly.assertThat(team.getName()).isEqualTo(result.getName());
            softly.assertThat(team.getPlayers()).isEqualTo(result.getPlayers());
            softly.assertThat(team).isEqualTo(result);
        });
    }

    @Test
    public void saveAndGetAllOne() {
        var expected = buildTeamList();
        expected.stream().forEach(entity -> teamDao.create(entity));

        var result = teamDao.readAll();
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result).hasSameSizeAs(expected);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result).hasSameElementsAs(expected);
        });
    }

    @Test
    public void saveAndGetAllMultiple() {
        var team = buildTeam();
        List<Team> expected = new ArrayList<>();
        expected.add(team);

        teamDao.create(team);
        var result = teamDao.readAll();
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result).hasSize(1);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result).hasSameElementsAs(expected);
        });
    }

    @Test
    public void updateOne() {
        var expected = storeOne(buildTeam());

        expected.setName("Updated Name");
        teamDao.update(expected);
        var result = teamDao.read(expected.getId()).get();
        Assertions.assertThat(result).isNotNull();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getName()).isEqualTo(expected.getName());
        });
    }

    @Test
    public void deleteOneOfOne() {
        var expected = storeOne(buildTeam());

        teamDao.delete(expected);
        var result = teamDao.readAll();
        Assertions.assertThat(result).isNotNull();
        Assertions.assertThat(result).hasSize(0);
    }

    @Test
    public void deleteOneOfMany() {
        var expected = storeAll(buildTeamList());

        teamDao.delete(expected.get(2));
        expected.remove(2);

        var result = teamDao.readAll();
        Assertions.assertThat(result).isNotNull();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result).hasSameSizeAs(expected);
            softly.assertThat(result).hasSameElementsAs(expected);
        });
    }

    @Test
    public void saveNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamDao.create(null)
        );
    }

    @Test
    public void updateNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamDao.update(null)
        );
    }

    @Test
    public void deleteNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> teamDao.delete(null)
        );
    }

    @Test
    public void deleteNonExisting() {
        var entity = buildTeam();
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> teamDao.delete(entity)
        );
        entityManager.flush();
    }

    @Test
    public void getEmpty() {
        Assertions.assertThat(teamDao.read(1)).isEmpty();
    }

    @Test
    public void getAllEmpty() {
        Assertions.assertThat(teamDao.readAll()).isEmpty();
    }
}
