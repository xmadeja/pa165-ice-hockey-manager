package cz.muni.fi.pa165.icehockeymanager.dao;

import cz.muni.fi.pa165.icehockeymanager.config.PersistanceApplicationConfig;
import cz.muni.fi.pa165.icehockeymanager.model.Game;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = PersistanceApplicationConfig.class)
@Transactional
public class GameDaoTest {

    @Autowired
    private GameDao gameDao;

    @Autowired
    private TeamDao teamDao;

    @Autowired
    private EntityManager entityManager;

    private Game buildGame() {
        var game = new Game();
        game.setGameDateTime(ZonedDateTime.now());
        game.setHomeTeam(storeOne(buildHomeTeam()));
        game.setAwayTeam(storeOne(buildAwayTeam()));
        return game;
    }

    private Team buildAwayTeam() {
        return new Team("Chicago Blackhawks");
    }

    private Team buildHomeTeam() {
        return new Team("Boston Bruins");
    }

    private List<Game> buildGameList() {
        var gamees = new ArrayList<Game>();
        gamees.add(buildGame());
        gamees.add(buildGame());
        gamees.add(buildGame());
        return gamees;
    }

    private Game storeOne(Game game) {
        gameDao.create(game);
        return game;
    }

    private Team storeOne(Team team) {
        teamDao.create(team);
        return team;
    }

    private List<Game> storeAll(List<Game> games) {
        games.stream().forEach(entity -> gameDao.create(entity));
        return games;
    }

    @Test
    public void saveAndGet() {
        var game = buildGame();
        gameDao.create(game);
        var result = gameDao.read(game.getId()).get();
        assertThat(result).isNotNull();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(game.getId()).isEqualTo(result.getId());
            softly.assertThat(game.getAwayTeam()).isEqualTo(result.getAwayTeam());
            softly.assertThat(game.getHomeTeam()).isEqualTo(result.getHomeTeam());
            softly.assertThat(game.getAwayTeamScore()).isEqualTo(result.getAwayTeamScore());
            softly.assertThat(game.getHomeTeamScore()).isEqualTo(result.getAwayTeamScore());
            softly.assertThat(game).isEqualTo(result);
        });
    }

    @Test
    public void saveWithTeamsAndGet() {
        var awayTeam = storeOne(buildAwayTeam());
        var homeTeam = storeOne(buildHomeTeam());
        var game = buildGame();
        game.setAwayTeam(awayTeam);
        game.setHomeTeam(homeTeam);
        gameDao.create(game);
        var result = gameDao.read(game.getId()).get();
        assertThat(result).isNotNull();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(game.getId()).isEqualTo(result.getId());
            softly.assertThat(game.getAwayTeam()).isEqualTo(result.getAwayTeam());
            softly.assertThat(game.getHomeTeam()).isEqualTo(result.getHomeTeam());
            softly.assertThat(game.getAwayTeamScore()).isEqualTo(result.getAwayTeamScore());
            softly.assertThat(game.getHomeTeamScore()).isEqualTo(result.getAwayTeamScore());
            softly.assertThat(game).isEqualTo(result);
        });
    }

    @Test
    public void saveAndGetAllOne() {
        var expected = buildGameList();
        expected.stream().forEach(entity -> gameDao.create(entity));

        var result = gameDao.readAll();
        assertThat(result).isNotNull();
        assertThat(result).hasSameSizeAs(expected);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result).hasSameElementsAs(expected);
        });
    }

    @Test
    public void saveAndGetAllMultiple() {
        var game = buildGame();
        List<Game> expected = new ArrayList<>();
        expected.add(game);

        gameDao.create(game);
        var result = gameDao.readAll();
        assertThat(result).isNotNull();
        assertThat(result).hasSize(1);
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result).hasSameElementsAs(expected);
        });
    }

    @Test
    public void updateOne() {
        var expected = storeOne(buildGame());

        expected.setHomeTeamScore(3);
        expected.setAwayTeamScore(1);
        gameDao.update(expected);
        var result = gameDao.read(expected.getId()).get();
        assertThat(result).isNotNull();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result.getHomeTeamScore()).isEqualTo(expected.getHomeTeamScore());
            softly.assertThat(result.getAwayTeamScore()).isEqualTo(expected.getAwayTeamScore());
        });
    }

    @Test
    public void saveNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> gameDao.create(null)
        );
    }

    @Test
    public void updateNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> gameDao.update(null)
        );
    }

    @Test
    public void deleteNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(
                () -> gameDao.delete(null)
        );
    }

    @Test
    public void deleteNonExisting() {
        var entity = buildGame();
        assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(
                () -> gameDao.delete(entity)
        );
        entityManager.flush();

    }

    @Test
    public void deleteOneOfOne() {
        var expected = storeOne(buildGame());

        gameDao.delete(expected);
        var result = gameDao.readAll();
        assertThat(result).isNotNull();
        assertThat(result).hasSize(0);
    }

    @Test
    public void deleteOneOfMany() {
        var expected = storeAll(buildGameList());

        gameDao.delete(expected.get(1));
        expected.remove(1);
        var result = gameDao.readAll();
        assertThat(result).isNotNull();
        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(result).hasSameSizeAs(expected);
            softly.assertThat(result).hasSameElementsAs(expected);
        });
    }

}
