package cz.muni.fi.pa165.icehockeymanager.dao;

import cz.muni.fi.pa165.icehockeymanager.model.Game;
import org.springframework.stereotype.Repository;

@Repository
public class GameDao extends AbstractDao<Game> {

    public GameDao() {
        super(Game.class);
    }
}
