package cz.muni.fi.pa165.icehockeymanager.dao;

import java.util.List;
import java.util.Optional;

/**
 * Interface for all Dao classes.
 *
 * @param <T> Class of the managed model.
 */
public interface Dao<T> {

    /**
     * Retrieve entity by id.
     *
     * @param id valid database id
     * @return entity, or empty optional if none was found for the given id
     */
    Optional<T> read(long id);

    /**
     * Retrieve all instances managed by the given DAO.
     *
     * @return List of all related entities.
     */
    List<T> readAll();

    /**
     * Store entity.
     *
     * @param t New non-null entity
     */
    void create(T t);

    /**
     * Uodate the entity in storage
     *
     * @param t Existing non-null entity
     */
    void update(T t);

    /**
     * Delete the enity from storage
     *
     * @param t Existing non-null entity
     */
    void delete(T t);
}
