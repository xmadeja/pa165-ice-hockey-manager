package cz.muni.fi.pa165.icehockeymanager.model;

import cz.muni.fi.pa165.icehockeymanager.properties.DirtyHash;
import cz.muni.fi.pa165.icehockeymanager.properties.IDEqual;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.Optional;

/**
 * Representation of ice hockey game.
 * <p>
 * Secondary HashCode attributes - gameDateTime
 */
@Entity
@Table
@Setter @Getter
public class Game implements IDEqual, DirtyHash {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotNull
    @ManyToOne(optional = false)
    private Team homeTeam;

    @NotNull
    @ManyToOne(optional = false)
    private Team awayTeam;

    @NotNull
    @Column(nullable = false)
    private ZonedDateTime gameDateTime;

    @PositiveOrZero
    @Column(nullable = false)
    private int homeTeamScore;

    @PositiveOrZero
    @Column(nullable = false)
    private int awayTeamScore;

    @ManyToOne
    private Team winner;

    public Game() {
    }

    public Game(Team homeTeam, Team awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public Optional<Team> getWinner() {
        return Optional.ofNullable(winner);
    }

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", homeTeam=" + homeTeam +
                ", awayTeam=" + awayTeam +
                ", gameDateTime=" + gameDateTime +
                ", homeTeamScore=" + homeTeamScore +
                ", awayTeamScore=" + awayTeamScore +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Game)) {
            return false;
        }
        Game other = (Game) o;
        return getId() != null && getId().equals(other.getId());
    }

    @Override
    public int hashCode() {
        if (getId() != null) {
            return getId().hashCode();
        }
        return Objects.hash(getGameDateTime());
    }
}
