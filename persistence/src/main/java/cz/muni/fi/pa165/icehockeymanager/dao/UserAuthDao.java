package cz.muni.fi.pa165.icehockeymanager.dao;

import cz.muni.fi.pa165.icehockeymanager.model.Account;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.Optional;

@Repository
public class UserAuthDao extends AbstractDao<Account>{

    public UserAuthDao() {
        super(Account.class);
    }

    public Optional<Account> findByUsername(String username) {
        Account user = null;
        try {
            user = (Account) entityManager
                    .createQuery(
                            "SELECT a FROM Account a WHERE a.username = :username",
                            Account.class
                    )
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException ignored) {

        }
        return Optional.ofNullable(user);
    }
}
