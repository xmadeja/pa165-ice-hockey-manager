package cz.muni.fi.pa165.icehockeymanager.exceptions;

public class UnknownPlayerException extends RuntimeException {
    public UnknownPlayerException() {
    }

    public UnknownPlayerException(String message) {
        super(message);
    }

    public UnknownPlayerException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownPlayerException(Throwable cause) {
        super(cause);
    }
}
