package cz.muni.fi.pa165.icehockeymanager.model;


import cz.muni.fi.pa165.icehockeymanager.properties.DirtyHash;
import cz.muni.fi.pa165.icehockeymanager.properties.IDEqual;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Objects;
import java.util.Optional;

/**
 * Representation of ice hockey team.
 * <p>
 * Secondary HashCode attributes - name.
 */
@Entity
@Table
@Setter @Getter
public class Player implements IDEqual, DirtyHash {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotBlank
    @Column(nullable = false)
    private String name;

    @ManyToOne
    private Team team;

    public Player() {
    }

    public Player(String name, Team team) {
        Objects.requireNonNull(name);
        this.name = name;
        this.team = team;
    }

    public Optional<Team> getTeam() {
        return Optional.ofNullable(team);
    }

    public boolean isFree() {
        return getTeam().isEmpty();
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + getId() +
                ", name='" + getName() + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Player)) {
            return false;
        }
        Player other = (Player) o;
        return getId() != null && getId().equals(other.getId());
    }

    @Override
    public int hashCode() {
        if (getId() != null) {
            return getId().hashCode();
        }
        return Objects.hash(getName());
    }
}
