package cz.muni.fi.pa165.icehockeymanager.exceptions;

public class PlayerNotInTeamException extends RuntimeException {
    public PlayerNotInTeamException() {
    }

    public PlayerNotInTeamException(String message) {
        super(message);
    }

    public PlayerNotInTeamException(String message, Throwable cause) {
        super(message, cause);
    }

    public PlayerNotInTeamException(Throwable cause) {
        super(cause);
    }
}
