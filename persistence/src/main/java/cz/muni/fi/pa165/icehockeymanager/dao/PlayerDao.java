package cz.muni.fi.pa165.icehockeymanager.dao;

import cz.muni.fi.pa165.icehockeymanager.model.Player;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PlayerDao extends AbstractDao<Player> {

    public PlayerDao() {
        super(Player.class);
    }

    /**
     * Get all players not assigned to a team.
     *
     * @return List of all free players.
     */
    public List<Player> getAllFreePlayers() {
        return entityManager
                .createQuery("SELECT p FROM Player p WHERE p.team IS NULL", Player.class)
                .getResultList();
    }

}
