package cz.muni.fi.pa165.icehockeymanager.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public abstract class AbstractDao<T> implements Dao<T> {

    private final Class<T> entityClass;
    @PersistenceContext
    protected EntityManager entityManager;

    public AbstractDao(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public Optional<T> read(long id) {
        return Optional.ofNullable(entityManager.find(entityClass, id));
    }

    @Override
    public List<T> readAll() {
        var queryBuilder = entityManager.getCriteriaBuilder().createQuery(entityClass);
        var queryRoot = queryBuilder.from(entityClass);
        var query = queryBuilder.select(queryRoot);

        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public void create(T entity) {
        Objects.requireNonNull(entity);
        entityManager.persist(entity);
    }

    @Override
    public void update(T entity) {
        Objects.requireNonNull(entity);
        entityManager.merge(entity);
    }

    @Override
    public void delete(T entity) {
        Objects.requireNonNull(entity);
        if (!entityManager.contains(entity)) {
            throw new IllegalArgumentException("Trying to remove entity not stored in the database.");
        }
        var merged = entityManager.merge(entity);
        entityManager.remove(merged);
    }


}
