package cz.muni.fi.pa165.icehockeymanager.dao;

import cz.muni.fi.pa165.icehockeymanager.model.Account;
import cz.muni.fi.pa165.icehockeymanager.model.Team;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.Optional;

@Repository
public class TeamDao extends AbstractDao<Team> {

    public TeamDao() {
        super(Team.class);
    }

    public Optional<Team> findByManager(Account manager) {
        Team team = null;
        try {
            team = (Team) entityManager
                    .createQuery(
                            "SELECT t FROM Team t WHERE t.managerId = :managerId",
                            Team.class
                    )
                    .setParameter("managerId", manager.getId())
                    .getSingleResult();
        } catch(NoResultException ignored) {

        }
        return Optional.ofNullable(team);
    }
}
