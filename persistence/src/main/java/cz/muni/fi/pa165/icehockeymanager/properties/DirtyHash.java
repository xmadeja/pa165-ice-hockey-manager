package cz.muni.fi.pa165.icehockeymanager.properties;

/**
 * Classes implementing this interface use ID to compute hash.
 * <p>
 * If ID is not set, they use a set of secondary attributes instead.
 * These attributes therefore must be treated as immutable
 * if you wish to depend on the hash value.
 */
public interface DirtyHash {
}
