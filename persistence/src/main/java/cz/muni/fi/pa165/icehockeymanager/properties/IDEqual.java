package cz.muni.fi.pa165.icehockeymanager.properties;

/**
 * Classes implementing this interface use ID attribute
 * for equality.
 * <p>
 * Two instances are equal iff both of their IDs are not null and equal.
 */
public interface IDEqual {
}
