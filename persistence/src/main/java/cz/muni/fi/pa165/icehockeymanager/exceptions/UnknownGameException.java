package cz.muni.fi.pa165.icehockeymanager.exceptions;

public class UnknownGameException extends RuntimeException{
    public UnknownGameException() {
    }

    public UnknownGameException(String message) {
        super(message);
    }

    public UnknownGameException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownGameException(Throwable cause) {
        super(cause);
    }
}
