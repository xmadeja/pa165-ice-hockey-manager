package cz.muni.fi.pa165.icehockeymanager.exceptions;

public class UnknownTeamException extends RuntimeException {
    public UnknownTeamException() {
    }

    public UnknownTeamException(String message) {
        super(message);
    }

    public UnknownTeamException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownTeamException(Throwable cause) {
        super(cause);
    }
}
