package cz.muni.fi.pa165.icehockeymanager.model;

import cz.muni.fi.pa165.icehockeymanager.exceptions.UnknownGameException;
import cz.muni.fi.pa165.icehockeymanager.properties.DirtyHash;
import cz.muni.fi.pa165.icehockeymanager.properties.IDEqual;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Representation of ice hockey team.
 * <p>
 * Secondary HashCode attributes - name
 */
@Entity
@Table
@Setter @Getter
public class Team implements IDEqual, DirtyHash {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @NotBlank
    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "team")
    private Set<Player> players = new HashSet<>();

    @OneToMany(mappedBy = "homeTeam")
    private Set<Game> homeGames = new HashSet<>();

    @OneToMany(mappedBy = "awayTeam")
    private Set<Game> awayGames = new HashSet<>();

    @Column(nullable = true)
    private Long managerId;

    public Team() {
    }

    public Team(String name) {
        Objects.requireNonNull(name);
        setName(name);
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public void removePlayer(Player player) {
        players.remove(player);
    }

    public boolean containsPlayer(Player player) {
        return players.contains(player);
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Team)) {
            return false;
        }
        Team other = (Team) o;
        return getId() != null && getId().equals(other.getId());
    }

    @Override
    public int hashCode() {
        if (getId() != null) {
            return getId().hashCode();
        }
        return Objects.hash(getName());
    }

    public Set<Player> getPlayers() {
        return new HashSet<>(players);
    }

    public void setPlayers(Set<Player> players) {
        this.players = new HashSet<>(players);
    }

    public Set<Game> getHomeGames() {
        return new HashSet<>(homeGames);
    }

    public void setHomeGames(Set<Game> homeGames) {
        this.homeGames = new HashSet<>(homeGames);
    }

    public Set<Game> getAwayGames() {
        return new HashSet<>(awayGames);
    }

    public void setAwayGames(Set<Game> awayGames) {
        this.awayGames = new HashSet<>(awayGames);
    }

    public void addHomeGame(Game game) {
        homeGames.add(game);
    }

    public void addAwayGame(Game game) {
        awayGames.add(game);
    }

    public void removeHomeGame(Game game) {
        if (!homeGames.contains(game)) {
            throw new UnknownGameException("Game not found as home game of team " + name);
        }
        homeGames.remove(game);
    }

    public void removeAwayGame(Game game) {
        if (!awayGames.contains(game)) {
            throw new UnknownGameException("Game not found as away game of team " + name);
        }
        awayGames.remove(game);
    }
}
